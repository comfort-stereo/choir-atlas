using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;

public class InputManager {
    
    public const float MinAnalogMagnitude = 0.35f;
    
    static readonly string[] ButtonInputNames = {
        "A",
        "B",
        "X",
        "Y",
        "Left Bumper",
        "Right Bumper",
        "Start",
        "Left Stick Click",
        "Right Stick Click"
    };

    class ButtonInput {
        public bool Down { get; set; }
        public bool Up { get; set; }
        public bool Held { get; set; }
    }
    
    public Vector2 AnalogLeft { get; private set; }
    public Vector2 AnalogRight { get; private set; }

    public Vector2 AnalogLeftDirection {
        get { return this.AnalogLeft.normalized; }
    }

    public Vector2 AnalogRightDirection {
        get { return this.AnalogRight.normalized; }
    }

    public bool HasAnalogLeft {
        get { return this.AnalogLeft != Vector2.zero; }
    }

    public bool HasAnalogRight {
        get { return this.AnalogRight != Vector2.zero; }
    }
    
    public bool HasAnalogLeftX {
        get { return Mathf.Abs(this.AnalogLeft.x) > MinAnalogMagnitude; }
    }
    
    public bool HasAnalogRightX {
        get { return Mathf.Abs(this.AnalogRight.x) > MinAnalogMagnitude; }
    }
    
    public bool HasAnalogLeftY {
        get { return Mathf.Abs(this.AnalogLeft.y) > MinAnalogMagnitude; }
    }
    
    public bool HasAnalogRightY {
        get { return Mathf.Abs(this.AnalogRight.y) > MinAnalogMagnitude; }
    }

    Dictionary<string, ButtonInput> ButtonInputs { get; set; }

    public InputManager() {
        this.ButtonInputs = ButtonInputNames.ToDictionary(name => name, name => new ButtonInput());
    }

    public void Update() {
        this.AnalogLeft = ToAnalog(Input.GetAxisRaw("Horizontal Left"), -Input.GetAxisRaw("Vertical Left"));
        this.AnalogRight = ToAnalog(Input.GetAxisRaw("Horizontal Right"), -Input.GetAxisRaw("Vertical Right"));
        
        foreach (var name in ButtonInputNames) {
            var input = this.ButtonInputs[name];
            input.Down = Input.GetButtonDown(name);
            input.Up = Input.GetButtonUp(name);
            input.Held = Input.GetButton(name);
        }
    }

    Vector2 ToAnalog(float x, float y) {
        var analog = new Vector2(x, y);
        if (analog.sqrMagnitude < MinAnalogMagnitude.Squared()) {
            return Vector2.zero;
        }
        return analog;
    }

    public bool ButtonDown(string name, bool consume = true) {
        var input = this.ButtonInputs.GetOrNull(name);
        var value = input.Down;
        if (consume) {
            input.Down = false;
        }
        return value;
    }

    public bool ButtonUp(string name, bool consume = true) {
        var input = this.ButtonInputs.GetOrNull(name);
        var value = input.Up;
        if (consume) {
            input.Up = false;
        }
        return value;
    }

    public bool ButtonHeld(string name) {
        return this.ButtonInputs[name].Held;
    }
}
