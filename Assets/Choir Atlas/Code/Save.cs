using Sirenix.Serialization;
using Utilities;

public class CharacterData {

    [OdinSerialize] public string Name { get; private set; } 
    [OdinSerialize] public string LastWaypointID { get; private set; } 
    [OdinSerialize] public string LastPlaceID { get; private set; } 

    public CharacterData() {
        this.Name = "";
        this.LastWaypointID = "";
        this.LastPlaceID = "";
    }
}

public class WorldData {
    [OdinSerialize] public string[] ActiveWaypointIDs { get; private set; }

    public WorldData() {
        this.ActiveWaypointIDs = new string[0]; 
    }
}

public class Save {

    [OdinSerialize] public CharacterData CharacterData { get; private set; }
    [OdinSerialize] public WorldData WorldData { get; private set; }

    public Save() {
        this.CharacterData = new CharacterData();
        this.WorldData = new WorldData();
    }
}
