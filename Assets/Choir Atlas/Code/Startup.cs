﻿using Sirenix.Serialization;

public class Startup {
    
    [OdinSerialize] public GameState State { get; private set; } 
    [OdinSerialize] public Save Save { get; private set; } 

    public Startup() {
        this.State = GameState.Play;
        this.Save = new Save();
    }
}
