﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Utilities.Containers;

public class PlaceableTriggerZone : SerializedMonoBehaviour {

    [OdinSerialize, Required] TriggerZone TriggerZone { get; set; } = new BoxTriggerZone();

    public bool HasTrigger => this.TriggerZone.DetectTrigger(this.transform.position) != null;

    void OnDrawGizmosSelected() {
        this.TriggerZone.DrawGizmo(this.transform.position);
    }

    public ArraySlice<Collider2D> DetectTriggers() {
        return this.TriggerZone.DetectTriggers(this.transform.position);
    }

    public Collider2D DetectTrigger() {
        return this.TriggerZone.DetectTrigger(this.transform.position);
    }
}
