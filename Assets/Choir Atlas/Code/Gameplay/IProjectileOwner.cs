﻿using UnityEngine;

public interface IProjectileOwner {
    void OnProjectileHit(ProjectileBehaviour behaviour, GameObject obj);
    void OnProjectileDetonate(ProjectileBehaviour behaviour);
    void OnProjectileFired(ProjectileBehaviour behaviour, Vector2 direction);
}
