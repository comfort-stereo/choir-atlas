﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;

public enum ActionZoneDrawMode {
    Never,
    Active,
    Always
}

public class ActionZone {
    [OdinSerialize, Required] public TriggerZone TriggerZone { get; set; } = new BoxTriggerZone();
    [OdinSerialize, MinValue(0f), MaxValue(1f)] public float StartTime { get; private set; }
    [OdinSerialize, MinValue(0f), MaxValue(1f)] public float EndTime { get; private set; }
    [OdinSerialize] public string ID { get; private set; }
}
