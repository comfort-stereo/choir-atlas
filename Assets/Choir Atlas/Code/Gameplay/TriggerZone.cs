﻿using ChoirUtilities;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Utilities;
using Utilities.Containers;

public abstract class TriggerZone {
    
    public const int ColliderBufferSize = 6;
    
    [OdinSerialize] public Vector2 Offset { get; set; }

    protected Collider2D[] ColliderBuffer { get; set; } 

    #if UNITY_EDITOR
    [Button]
    public void Edit() {
        TriggerZoneTool.BeginEdit(this);
    }
    #endif

    public ArraySlice<Collider2D> DetectTriggers(Vector2 origin, HorizontalDirection facing = HorizontalDirection.Right, int mask = LayerMasks.Standard) {
        if (!this.CanBeTriggered) {
            return ArraySlice<Collider2D>.Empty;
        }
        if (this.ColliderBuffer == null) {
            this.ColliderBuffer = new Collider2D[ColliderBufferSize];
        }
        var slice = DetectTriggersInternal(origin, facing, mask);
        for (var i = slice.Length; i < this.ColliderBuffer.Length; i++) {
            this.ColliderBuffer[i] = null;
        }
        return slice;
    }

    public Collider2D DetectTrigger(Vector2 origin, HorizontalDirection facing = HorizontalDirection.Right, int mask = LayerMasks.Standard) {
        if (this.CanBeTriggered) {
            return DetectTriggerInternal(origin, facing, mask);
        }
        return null;
    }

    public void DrawGizmo(Vector2 origin, HorizontalDirection facing = HorizontalDirection.Right) {
        if (this.CanBeTriggered) {
            GizmoUtility.StartColor(IsTriggered(origin, facing) ? Color.red : Color.cyan);
            DrawGizmoInternal(origin, facing);
            GizmoUtility.EndColor();
        }
    }

    public bool IsTriggered(Vector2 center, HorizontalDirection facing = HorizontalDirection.Right, int mask = LayerMasks.Standard) {
        if (this.CanBeTriggered) {
            return DetectTrigger(center, facing, mask) != null;
        }
        return false;
    }

    public Vector2 OffsetByFacing(HorizontalDirection facing) {
        var offset = this.Offset;
        if (facing == HorizontalDirection.Left) {
            offset.x = -offset.x;
        }
        return offset;
    }

    public void SetBounds(Vector2 start, Vector2 end) {
        if (start.x > end.x) {
            var temporary = start.x;
            start.x = end.x;
            end.x = temporary;
        }
        if (start.y > end.y) {
            var temporary = start.y;
            start.y = end.y;
            end.y = temporary;
        }
        SetBoundsInternal(start, end);
    }
    
    public abstract bool CanBeTriggered { get; }
    
    protected abstract void SetBoundsInternal(Vector2 start, Vector2 end);
    protected abstract ArraySlice<Collider2D> DetectTriggersInternal(Vector2 origin, HorizontalDirection facing, int mask);
    protected abstract Collider2D DetectTriggerInternal(Vector2 origin, HorizontalDirection facing, int mask);
    protected abstract void DrawGizmoInternal(Vector2 origin, HorizontalDirection facing);
}

public class CircleTriggerZone : TriggerZone {
    [OdinSerialize] public float Radius { get; set; }

    public override bool CanBeTriggered {
        get { return this.Radius != 0f; }
    }
    
    protected override void SetBoundsInternal(Vector2 start, Vector2 end) {
        this.Radius = Mathf.Max(end.x - start.x, end.y - start.y);
        this.Offset = new Vector2(start.x + this.Radius, start.y + this.Radius);
    }

    protected override ArraySlice<Collider2D> DetectTriggersInternal(Vector2 origin, HorizontalDirection facing, int mask) {
        var count = Physics2D.OverlapCircleNonAlloc(origin + OffsetByFacing(facing), this.Radius, this.ColliderBuffer, mask);
        return new ArraySlice<Collider2D>(this.ColliderBuffer, 0, count);
    }

    protected override Collider2D DetectTriggerInternal(Vector2 origin, HorizontalDirection facing, int mask) {
        return Physics2D.OverlapCircle(origin + OffsetByFacing(facing), this.Radius, mask);
    }

    protected override void DrawGizmoInternal(Vector2 origin, HorizontalDirection facing) {
        Gizmos.DrawWireSphere(origin + OffsetByFacing(facing), this.Radius);
    }
}

public class BoxTriggerZone : TriggerZone {
    [OdinSerialize] public Vector2 Size { get; set; }

    public override bool CanBeTriggered {
        get { return this.Size != Vector2.zero; }
    }

    protected override void SetBoundsInternal(Vector2 start, Vector2 end) {
        this.Size = new Vector2(end.x - start.x, end.y - start.y ); 
        this.Offset = new Vector2(start.x + this.Size.x / 2f, start.y + this.Size.y / 2f);
    }

    protected override ArraySlice<Collider2D> DetectTriggersInternal(Vector2 origin, HorizontalDirection facing, int mask) {
        var count = Physics2D.OverlapBoxNonAlloc(origin + OffsetByFacing(facing), this.Size, 0f, this.ColliderBuffer, mask);
        return new ArraySlice<Collider2D>(this.ColliderBuffer, 0, count);
    }

    protected override Collider2D DetectTriggerInternal(Vector2 origin, HorizontalDirection facing, int mask) {
        return Physics2D.OverlapBox(origin + OffsetByFacing(facing), this.Size, 0f, mask);
    }

    protected override void DrawGizmoInternal(Vector2 origin, HorizontalDirection facing) {
        Gizmos.DrawWireCube(origin + OffsetByFacing(facing), this.Size);
    }
}

