﻿using Sirenix.Serialization;
using UnityEngine;

public class HitInfo {
    [OdinSerialize] public int Damage { get; set; }
    [OdinSerialize] public float Force { get; set; }

    public Hit ToHit() => new Hit { Damage = this.Damage, Force = this.Force };
    public Hit ToHit(Vector2 direction) => new Hit { Damage = this.Damage, Force = this.Force, Direction = direction };
}

public struct Hit {
    public int Damage;
    public float Force;
    public Vector2 Direction;
}
