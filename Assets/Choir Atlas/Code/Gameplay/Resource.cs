﻿using System;
using Sirenix.Serialization;
using UnityEngine;

public class Resource {

    public int Max {
        get { return this.max; }
        set { this.max = Math.Max(0, value); }
    }
    [OdinSerialize] int max = 1;

    public int Current {
        get { return this.current; }
        set { this.current = Mathf.Min(Math.Max(0, value), this.max); }
    }
    [OdinSerialize] int current = 1;

    public bool IsDepleted => this.Current == 0;

    public Resource(int max = 1, int current = 1) {
        this.Max = max;
        this.Current = current;
    }
}
