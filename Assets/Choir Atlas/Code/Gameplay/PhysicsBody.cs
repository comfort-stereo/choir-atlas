﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Utilities;

[RequireComponent(typeof(Collider2D))]
public class PhysicsBody : SerializedMonoBehaviour {

    const int ContactBufferSize = 5;
    const float Gravity = 6f;
    
    public Vector2 Velocity { get; set; }
    public bool IsStatic { get; set; }
    
    Vector2 AppendedMovement { get; set; }
    
    public bool EnableGravity { get; set; } 
    public bool UseCustomGravity { get; set; }
    
    public float CustomGravity { get; set; }
    
    public Collider2D Collider { get; private set; }
    
    Rigidbody2D Rigidbody { get; set; }
    ContactPoint2D[] ContactBuffer { get; set; }

    public Vector2 Position {
        get { return this.Rigidbody.position; }
        set { this.Rigidbody.position = value; }
    }

    public float Rotation {
        get { return this.Rigidbody.rotation; }
        set { this.Rigidbody.rotation = value; }
    }

    public bool FreezeRotation { 
        get { return this.Rigidbody.freezeRotation; }
        set { this.Rigidbody.freezeRotation = value; }
    }

    public float Mass {
        get { return this.Rigidbody.mass; }
        set { this.Rigidbody.mass = value; }
    }

    public bool IsMoving => this.Velocity != Vector2.zero;
    public bool IsNotMoving => this.Velocity == Vector2.zero;

    void Move(Vector2 offset) {
        this.Rigidbody.MovePosition(this.Position + offset + this.AppendedMovement);
        this.AppendedMovement = Vector2.zero;
    }

    void Awake() {
        this.Collider = GetComponent<Collider2D>(); 
        this.ContactBuffer = new ContactPoint2D[ContactBufferSize];
        SetupRigidbody();
    }

    void FixedUpdate() {
        if (this.IsStatic) {
            this.Velocity = Vector2.zero;
            return;
        }
        
        float gravity;
        if (this.EnableGravity) {
            if (this.UseCustomGravity) {
                gravity = this.CustomGravity;
            } else {
                gravity = Gravity;
            }
        } else {
            gravity = 0f;
        }
        
        this.Velocity += gravity * Vector2.down * Timing.Delta;
        
        Move(this.Velocity * Timing.Delta);
        
        LimitVelocity();
    }

    void LimitVelocity() {
        
        for (var i = 0; i < this.Collider.GetContacts(this.ContactBuffer); i++) {
            var normal = this.ContactBuffer[i].normal;
            var velocity = this.Velocity;
            
            var vertical = Math.Abs(Vector3.Angle(Vector3.up, normal));
            if (vertical <= 60f) {
                velocity.y = Math.Max(velocity.y, 0f);
            } else if (vertical >= 120f) {
                velocity.y = Math.Min(velocity.y, 0f);
            } else {
                var horizontal = Math.Abs(Vector3.Angle(Vector3.right, normal));
                if (horizontal <= 60f) {
                    velocity.x = Math.Max(velocity.x, 0f);
                } else if (horizontal >= 120f) {
                    velocity.x = Math.Min(velocity.x, 0f);
                } 
            }
            
            this.Velocity = velocity;
        }
    }

    void SetupRigidbody() {
        this.Rigidbody = this.gameObject.AddOrReplaceComponent<Rigidbody2D>();
        this.Rigidbody.freezeRotation = true;
        this.Rigidbody.drag = 0f;
        this.Rigidbody.angularDrag = 0f;
        this.Rigidbody.gravityScale = 0f;
        this.Rigidbody.interpolation = RigidbodyInterpolation2D.Interpolate;
        this.Rigidbody.velocity = Vector2.zero;
    }

    public void AppendMovement(Vector2 movement) {
        this.AppendedMovement += movement;
    }
}
