﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Utilities;

public class AnimationBit : SerializedMonoBehaviour {
    
    public AnimationClip CurrentAnimation { get; private set; }
    
    Animator Animator { get; set; }
    Dictionary<AnimationClip, AnimatorOverrideController> AnimationOverrideControllerCache { get; } = new Dictionary<AnimationClip, AnimatorOverrideController>();
    AnimatorOverrideController AnimatorOverrideController => (AnimatorOverrideController) this.Animator.runtimeAnimatorController;

    void Awake() {
        SetupAnimator();
    }

    public void Animate(AnimationClip animation, float duration, float speed = 1f) {
        this.Animator.speed = (animation.length / duration) * speed;
        
        if (this.CurrentAnimation == animation) {
            return;
        }
        
        this.CurrentAnimation = animation;
        
        var controller = this.AnimatorOverrideController; 
        var original = controller.runtimeAnimatorController;
        var replacement = this.AnimationOverrideControllerCache.GetOrNull(animation);
        
        if (replacement == null) {
            replacement = new AnimatorOverrideController();
            replacement.runtimeAnimatorController = original;
            replacement["Current"] = animation;
            this.AnimationOverrideControllerCache[animation] = replacement;
        }

        /* 
         * Known issue with Unity. Need to set this to null before assignment, otherwise the new override controller will not be set up 
         * correctly.
         */ 
        this.Animator.runtimeAnimatorController = null;
        this.Animator.runtimeAnimatorController = replacement;
        this.Animator.Play("Current");
    }

    public void Clear() {
        Animate(Game.Instance.AssetReferenceTable.BlankAnimation, 1f);
    }

    public void MoveToNearestPixel(Vector2 position) {
        this.transform.position = position.ToNearestPixel();
    }

    void SetupAnimator() {
        this.Animator = this.gameObject.AddOrReplaceComponent<Animator>();
        this.Animator.runtimeAnimatorController = Game.Instance.AssetReferenceTable.DefaultAnimationOverrideController;
    }
}
