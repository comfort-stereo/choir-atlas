using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using ChoirUtilities;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Utilities;

public enum PuppetLockLevel {
    None,
    Low,
    High
}

public delegate void OnActionZoneDelegate(string id, GameObject obj);

[RequireComponent(typeof(Collider2D))]
public class Puppet : SerializedMonoBehaviour {

    public const float MaxGroundedYVelocity = 1f;

    const string MainAnimationBitName = "Main";

    public IProjectileOwner ProjectileOwner { get; set; }

    [OdinSerialize] Sprite Preview { get; set; }
    [OdinSerialize] public bool IsStatic { get; set; }

    [OdinSerialize, HideIf(nameof(IsStatic))] public float Mass { get; set; } = 1;
    [OdinSerialize, HideIf(nameof(IsStatic))] public bool UseCustomGravity { get; set; }
    [OdinSerialize, HideIf(nameof(IsStatic))] public bool EnableGravity { get; set; } = true;
    [OdinSerialize, ShowIf(nameof(UseCustomGravity)), HideIf(nameof(IsStatic))] float CustomGravity { get; set; }
    
    [OdinSerialize, Required] CircleTriggerZone RoofDetector { get; set; } = new CircleTriggerZone();
    [OdinSerialize, Required] CircleTriggerZone GroundDetector { get; set; } = new CircleTriggerZone();
    [OdinSerialize, Required] BoxTriggerZone WallDetector { get; set; } = new BoxTriggerZone(); 
    
    public PhysicsBody Body { get; private set; }
    public Vector2 GroundPosition { get; private set; }
    
    public bool IsTouchingRoof { get; private set; }
    public bool IsTouchingGround { get; private set; }
    public bool FrontIsTouchingWall { get; private set; }
    public bool BackIsTouchingWall { get; private set; }
    public bool IsBeingMovedByGround { get; private set; }
    
    public float CurrentMotionTime { get; private set; }
    
    public Motion CurrentMotion {
        get { return this.currentMotion; }
        set {
            if (this.currentMotion != value) {
                this.currentMotion?.OnDisable();
                this.currentMotion = value;
                this.currentMotion.OnEnable();
                this.CurrentMotionTime = 0f;
            }
        }
    }

    Motion currentMotion;
    
    Collider2D LastGround { get; set; }
    
    Dictionary<string, AnimationBit> AnimationBitMap { get; } = new Dictionary<string, AnimationBit>();

    public float CurrentMotionProgress {
        get {
            if (this.CurrentMotion == null) {
                return 1f;
            }
            return this.CurrentMotionTime / this.CurrentMotion.Duration;
        }
    }

    public bool CurrentMotionHasFinished => this.CurrentMotionProgress >= 1f;
    public bool IsTouchingWall => this.FrontIsTouchingWall || this.BackIsTouchingWall;
    public bool IsGrounded => this.IsTouchingGround && this.Body.Velocity.y < MaxGroundedYVelocity;
    
    public AnimationBit MainAnimationBit {
        get {
            #if UNITY_EDITOR
            if (!Application.isPlaying) {
                return null;
            }
            #endif
            return this.mainAnimationBit ?? (this.mainAnimationBit = GetAnimationBit(MainAnimationBitName));
        }
    }

    AnimationBit mainAnimationBit; 

    public AnimationBit GetAnimationBit(string id) {
        var bit = this.AnimationBitMap.GetOrNull(id);
        if (bit == null) {
            bit = this.gameObject.AddChildedAnimationBit(id + " Animation Bit" , "Entity");
            this.AnimationBitMap[id] = bit;
        }
        return bit;
    }

    public void ClearAllBitsExcept(string id) {
        foreach (var pair in this.AnimationBitMap) {
            if (pair.Key != id) {
                pair.Value.Clear();
            }
        }
    }

    public HorizontalDirection Facing {
        get { return this.facing; }
        set {
            if (this.facing != value) {
                this.facing = value;
                var degrees = this.facing == HorizontalDirection.Right ? 0f : 180f;
                this.transform.eulerAngles = this.transform.eulerAngles.Change(y: degrees);
            }
        }
    } 
    
    HorizontalDirection facing;
    
    void Awake() {
        this.Body = this.gameObject.AddPhysicsBody();
        this.Facing = HorizontalDirection.Right;
        DestroyPreview();
    }

    void OnDrawGizmos() {
        GizmoUtility.StartColor(Color.blue);
        if (!Application.isPlaying) {
            DrawPreview();
        }
        GizmoUtility.EndColor();
    }

    void DrawPreview() {
        var renderer = GetComponent<SpriteRenderer>();
        if (renderer == null) {
            this.gameObject.AddComponent<SpriteRenderer>();
        } else {
            renderer.sprite = this.Preview;
            renderer.hideFlags = HideFlags.DontSave | HideFlags.HideInInspector;
            renderer.sortingLayerName = "Front";
        }
    }

    void DestroyPreview() {
        var renderer = GetComponent<SpriteRenderer>();
        if (renderer != null) {
            Destroy(renderer);
        }
    }

    void OnDrawGizmosSelected() {
        var position = this.transform.position.To2();

        var layer = this.gameObject.layer;
        this.gameObject.layer = Layers.IgnoreRaycast;
        if (this.RoofDetector.CanBeTriggered) {
            this.RoofDetector.DrawGizmo(position, this.Facing);
        }
        if (this.GroundDetector.CanBeTriggered) {
            this.GroundDetector.DrawGizmo(position, this.Facing);
        }
        if (this.WallDetector.CanBeTriggered) {
            this.WallDetector.DrawGizmo(position, this.Facing);
        }
        this.gameObject.layer = layer;

        var advanced = this.CurrentMotion as AdvancedMotion;
        if (advanced != null) {
            for (var i = 0; i < advanced.ActionZones.Length; i++) {
                var zone = advanced.ActionZones[i];
                if (TriggerZoneIsActive(zone)) {
                    zone.TriggerZone.DrawGizmo(position, this.Facing);
                }
            }
        }
    }
    
    void FixedUpdate() {
        this.Body.IsStatic = this.IsStatic;
        this.Body.Mass = this.Mass;
        this.Body.UseCustomGravity = this.UseCustomGravity;
        this.Body.CustomGravity = this.CustomGravity;
        this.Body.EnableGravity = this.EnableGravity;
        
        var previousLayer = this.gameObject.layer;
        this.gameObject.layer = Layers.IgnoreRaycast;
        RoofDetectorUpdate();
        GroundDetectorUpdate();
        WallDetectorUpdate();
        this.gameObject.layer = previousLayer;

        UpdateMotionTime();
    }

    void UpdateMotionTime() {
        var motion = this.CurrentMotion;
        if (motion == null) {
            this.CurrentMotionTime = 0f;
            return;
        }
        this.CurrentMotionTime += Timing.Delta;

    }

    void LateUpdate() {
        foreach (var pair in this.AnimationBitMap) {
            pair.Value.MoveToNearestPixel(this.Body.Position);
        }
    }

    public void Do(Motion motion) {
        this.CurrentMotion = motion;
    }

    public void AnimateBit(string id, AnimationClip animation, float duration, float speed = 1f) {
        GetAnimationBit(id).Animate(animation, duration, speed);
    }

    public void Link(Motion motion, AnimationClip animation, float speed = 1f) {
        if (this.CurrentMotion == motion) {
            Animate(animation, motion.Duration, speed);
        }
    }
    public void LinkOverrideDuration(Motion motion, AnimationClip animation, float duration) {
        if (this.CurrentMotion == motion) {
            Animate(animation, duration);
        }
    }

    public void Animate(AnimationClip animation, float duration, float speed = 1f) {
        ClearAllBitsExcept("Main");
        this.MainAnimationBit.Animate(animation, duration, speed);
    }

    public void ClearBit(string id) {
        GetAnimationBit(id).Clear();
    }
    
    public bool TriggerZoneIsActive(ActionZone zone) {
        var progress = this.CurrentMotionProgress;
        return progress > zone.StartTime || progress < zone.EndTime;
    }

    public bool EmissionIsActive(EntityEmission emission) {
        if (emission.WasEmitted) {
            return false;
        }
        return this.CurrentMotionProgress > emission.EmittedTime;
    }

    void RoofDetectorUpdate() {
        this.IsTouchingRoof = this.RoofDetector.IsTriggered(this.transform.position, this.Facing);
    }
    
    void GroundDetectorUpdate() {
        var ground = this.GroundDetector.DetectTrigger(this.transform.position, this.Facing);
        
        this.IsTouchingGround = ground != null;
        this.IsBeingMovedByGround = false;

        var lastGroundPosition = this.GroundPosition;
        
        if (ground != null && ground.gameObject.HasFlag(GameObjectFlag.Obstacle | GameObjectFlag.Walkable)) {
            this.GroundPosition = ground.transform.position;
            if (ground == this.LastGround && this.GroundPosition != lastGroundPosition) {
                this.Body.AppendMovement(this.GroundPosition - lastGroundPosition);
                this.IsBeingMovedByGround = true;
            }
        }
        this.LastGround = ground;
    }
    
    void WallDetectorUpdate() {
        var position = this.transform.position;
        var colliders = this.WallDetector.DetectTriggers(position, this.Facing);
        
        this.FrontIsTouchingWall = false;
        this.BackIsTouchingWall = false;
        
        for (var i = 0; i < colliders.Length; i++) {
            var point = colliders[i].bounds.ClosestPoint(position);
            var difference = point.x - position.x;

            if (this.Facing == HorizontalDirection.Left) {
                difference = -difference;
            }
            
            if (difference > 0f) {
                this.FrontIsTouchingWall = true;
            } else {
                this.BackIsTouchingWall = true;
            }
        }
    }
}
