using Sirenix.OdinInspector;
using Sirenix.Serialization;

public class StartupScript : SerializedMonoBehaviour {
    
    [OdinSerialize] Startup startup = new Startup();

    void Awake() {
        Game.Instance.Run(this.startup);
    }

    void Update() {
        Destroy(this.gameObject);
    }
}
