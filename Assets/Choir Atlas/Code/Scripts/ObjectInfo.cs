﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Utilities;

[Flags]
public enum GameObjectFlag {
    Player = 1,
    Enemy = 2,
    Obstacle = 4,
    Walkable = 8,
    Pooled = 16,
    Decorative = 32
}

public interface IObjectInfo {
    bool HasFlag(GameObjectFlag flag);
    bool HasTag(string tag);
}

public class SimpleObjectInfo : IObjectInfo {
    [OdinSerialize] public GameObjectFlag Flags { get; set; }
    [OdinSerialize, Required] List<string> Tags { get; set; } = new List<string>();
    
    public bool HasFlag(GameObjectFlag flag) {
        return (this.Flags & flag) != 0;
    }

    public void SetFlag(GameObjectFlag flag, bool high) {
        if (high) {
            this.Flags |= flag;
        } else {
            this.Flags &= ~flag;
        }
    }

    public bool HasTag(string tag) {
        return this.Tags.Contains(tag);
    }

    public void SetTag(string tag) {
        this.Tags.AddIfUnique(tag);
    }

    public bool RemoveTag(string tag) {
        return this.Tags.Remove(tag);
    }
} 

public class ObjectInfo : SerializedMonoBehaviour, IObjectInfo {
    [OdinSerialize] public GameObjectFlag Flags { get; set; }
    [OdinSerialize, Required] List<string> Tags { get; set; } = new List<string>();
    
    public bool HasFlag(GameObjectFlag flag) {
        return (this.Flags & flag) != 0;
    }

    public void SetFlag(GameObjectFlag flag, bool high) {
        if (high) {
            this.Flags |= flag;
        } else {
            this.Flags &= ~flag;
        }
    }

    public bool HasTag(string tag) {
        return this.Tags.Contains(tag);
    }

    public void SetTag(string tag) {
        this.Tags.AddIfUnique(tag);
    }

    public bool RemoveTag(string tag) {
        return this.Tags.Remove(tag);
    }
}
