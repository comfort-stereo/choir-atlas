﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;

public abstract class Motion {
    [OdinSerialize] public float Duration { get; set; } = 1f;

    public abstract void OnEnable();
    public abstract void OnDisable();
}

public class BasicMotion : Motion {
    
    public BasicMotion() {
        this.Duration = 1f;
    }

    public BasicMotion(float duration) {
        this.Duration = duration;
    }
    
    public override void OnEnable() { }
    public override void OnDisable() { }
}

public class AdvancedMotion : Motion {
    [OdinSerialize, Required] public ActionZone[] ActionZones { get; set; } 
    [OdinSerialize, Required] public EntityEmission[] EntityEmissions { get; set; } 

    public AdvancedMotion() {
        this.Duration = 1f;
        this.ActionZones = new ActionZone[0];
        this.EntityEmissions = new EntityEmission[0];
    }

    public AdvancedMotion(float duration, ActionZone[] actionZones = null, EntityEmission[] entityEmissions = null) {
        this.Duration = duration;
        this.ActionZones = actionZones ?? new ActionZone[0];
        this.EntityEmissions = entityEmissions ?? new EntityEmission[0];
    }

    public override void OnEnable() { }
    
    public override void OnDisable() {
        for (var i = 0; i < this.EntityEmissions.Length; i++) {
            this.EntityEmissions[i].Reset();
        }
    }
}
