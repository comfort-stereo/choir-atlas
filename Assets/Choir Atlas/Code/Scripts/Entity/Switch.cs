﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class Switch : Entity {
    [OdinSerialize, Required] AnimationClip IdleOnAnimation { get; set; }
    [OdinSerialize, Required] AnimationClip IdleOffAnimation { get; set; }
    [OdinSerialize, Required] AnimationClip TurnOnAnimation { get; set; }
    [OdinSerialize, Required] AnimationClip TurnOffAnimation { get; set; }
    
    [OdinSerialize] ToggleBehaviour ToggleBehaviour { get; } = new ToggleBehaviour();
    
    protected override void OnAnimate(Motion motion) {
        this.Puppet.Link(this.ToggleBehaviour.IdleOnMotion, this.IdleOnAnimation);
        this.Puppet.Link(this.ToggleBehaviour.IdleOffMotion, this.IdleOffAnimation);
        this.Puppet.Link(this.ToggleBehaviour.TurnOnMotion, this.TurnOnAnimation);
        this.Puppet.Link(this.ToggleBehaviour.TurnOffMotion, this.TurnOffAnimation);
    }
}
