﻿
using ChoirUtilities;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Utilities;

public interface IPositionEditable {
    Vector2 EditablePosition { get; set; }
}

public abstract class EntityBehaviour {
    public Entity Entity { get; private set; }
    public Puppet Puppet => this.Entity.Puppet;
    public PhysicsBody Body => this.Entity.Puppet.Body;

    public void LinkTo(Entity entity) {
        this.Entity = entity;
    }
    
    public virtual void OnUpdate() { }
    public virtual void OnEnable() { }
    public virtual void OnDisable() { }
    public virtual void OnInitialize() { }
    public virtual void OnTriggerEnter(Collider2D collider) { }
}

public class BasicBehaviour : EntityBehaviour {
    [OdinSerialize, Required] public Motion Motion { get; set; }

    public void Do() {
        this.Puppet.Do(this.Motion);
    }
}

public class AdvancedBehaviour : EntityBehaviour {
    [OdinSerialize, Required] public AdvancedMotion Motion { get; set; }
    
    public void Do(Vector2 direction) {
        this.Puppet.Do(this.Motion);
    }

    public void Do() {
        this.Puppet.Do(this.Motion);
    }
}

public class DeathBehaviour : EntityBehaviour {
    [OdinSerialize] public Motion DeathMotion { get; set; }

    public void Die() {
        this.Puppet.Do(this.DeathMotion);
    }
}

public class HitBehaviour : EntityBehaviour {
    [OdinSerialize] public Motion LightHitMotion { get; set; }
    [OdinSerialize] public Motion HeavyHitMotion { get; set; }
    [OdinSerialize] public float HeavyHitRatio { get; set; }
    [OdinSerialize] public float HeavyHitBuildup { get; set; }
    [OdinSerialize] public float Poise { get; set; }

    public void OnHit(Hit hit) {
        this.Puppet.Body.Velocity = (1f - this.Poise) * hit.Force * hit.Direction;
        this.HeavyHitBuildup += hit.Damage / (float) this.Entity.Health.Current;
        
        if (this.LightHitMotion != null) {
            this.Puppet.Do(this.LightHitMotion);
        }
        
        if (this.HeavyHitBuildup > this.HeavyHitRatio) {
            if (this.HeavyHitMotion != null) {
                this.Puppet.Do(this.HeavyHitMotion);
            }
            this.HeavyHitBuildup = 0f;
        }

        if (this.Entity.IsInvulnerable) {
            return;
        }
        
        this.Entity.Health.Current -= hit.Damage;
    }
}

public class WallJumpBehaviour : EntityBehaviour {
    [OdinSerialize] public Vector2 JumpVelocity { get; set; }
    [OdinSerialize] public float JumpTime { get; set; } 
    [OdinSerialize] public float DelayPerSide { get; set; }
    
    [OdinSerialize] public Motion WallJumpMotion { get; set; }
    [OdinSerialize] public Motion OnWallMotion { get; set; }

    Timer WallJumpTimer { get; set; }
    Timer LeftDelayTimer { get; set; }
    Timer RightDelayTimer { get; set; }
    
    WalkingBehaviour WalkingBehaviour { get; set; } 
    
    public bool IsWallJumping => this.WallJumpTimer != null && this.WallJumpTimer.IsRunning;
    
    public override void OnInitialize() {
        this.WalkingBehaviour = this.Entity.GetBehaviour<WalkingBehaviour>();
        this.WallJumpTimer = new Timer(this.JumpTime, this.Entity);
        this.LeftDelayTimer = new Timer(this.DelayPerSide, this.Entity);
        this.RightDelayTimer = new Timer(this.DelayPerSide, this.Entity);
    }
    
    public override void OnUpdate() {
        if (this.Entity.IsGrounded) {
            this.WallJumpTimer.End();
            this.LeftDelayTimer.End();
            this.RightDelayTimer.End();
        }

        if (this.IsWallJumping) {
            this.Puppet.Do(this.WallJumpMotion);
        } else if (this.Entity.FrontIsOnWall) {
            this.Puppet.Do(this.OnWallMotion);
        }
    }
    
    public void WallJump(float multiplier = 1f) {
        var facingTimer = this.Puppet.Facing == HorizontalDirection.Right ? this.RightDelayTimer : this.LeftDelayTimer;
        var oppositeTimer = this.Puppet.Facing == HorizontalDirection.Right ? this.LeftDelayTimer : this.RightDelayTimer;

        if (facingTimer.IsRunning) {
            return;
        }

        this.Puppet.Do(this.WallJumpMotion);
        this.WallJumpTimer.Duration = this.JumpTime;
        
        var velocity = this.JumpVelocity * multiplier;

        if (this.Puppet.Facing == HorizontalDirection.Right) {
            velocity.x = -velocity.x;
        }
        
        this.Body.Velocity = velocity;

        facingTimer.Reset();
        oppositeTimer.End();
        
        this.WallJumpTimer.Reset();
    }
}

public class WalkingBehaviour : EntityBehaviour {
    [OdinSerialize] public float RunSpeed { get; set; }
    [OdinSerialize] public float AirRunSpeed { get; set; }
    [OdinSerialize] public float JumpSpeed { get; set; }
    
    [OdinSerialize, Required] public Motion IdleMotion { get; set; } = new BasicMotion();
    [OdinSerialize, Required] public Motion InAirMotion { get; set; } = new BasicMotion();
    [OdinSerialize, Required] public Motion RunMotion { get; set; } = new BasicMotion();
    [OdinSerialize] public Motion AirRunMotion { get; set; } 
    
    public void Idle(bool register = true) {
        if (register) {
            this.Puppet.Do(this.IdleMotion);
        }
    }
    
    public void Stop(bool register = true) {
        this.Body.Velocity = this.Body.Velocity.Change(x: 0f);
        Idle(register);
    }

    public void Run(HorizontalDirection direction, float multiplier = 1f, bool register = true) {
        if (register) {
            this.Puppet.Do(this.RunMotion);
        }
        this.Puppet.Facing = direction;
        this.Body.Velocity = direction.ToVector() * this.RunSpeed * multiplier;
    }
    
    public void Jump(Vector2 direction, float multiplier = 1f) {
        this.Puppet.Do(this.InAirMotion);
        this.Body.Velocity = this.Body.Velocity.Change(y: direction.y * this.JumpSpeed * multiplier);
        this.Body.Velocity = this.Body.Velocity + Vector2.right * direction.x * this.JumpSpeed * multiplier;
    }
    
    public void AirRun(HorizontalDirection direction, float multiplier = 1f, bool register = true) {
        if (register) {
            this.Puppet.Do(this.AirRunMotion ?? this.InAirMotion);
        }
        this.Puppet.Facing = direction;
        this.Body.Velocity = this.Body.Velocity.Change(x: direction.ToVector().x * this.AirRunSpeed * multiplier);
    }

    public void AirStop(bool register = true) {
        this.Body.Velocity = this.Body.Velocity.Change(x: 0f);
        AirIdle(register);
    }

    public void AirIdle(bool register = true) {
        if (register) {
            this.Puppet.Do(this.InAirMotion);
        }
    }
}

public class DashingBehaviour : EntityBehaviour {
    [OdinSerialize] public float VerticalSpeedMultiplier { get; set; }
    [OdinSerialize] public float DashSpeed { get; set; }
    [OdinSerialize] public float DashTime { get; set; } 
    
    [OdinSerialize, Required] public Motion Motion { get; set; }
    
    bool DashIsReady { get; set; }
    Timer DashTimer { get; set; }

    public bool CanDash {
        get { return this.DashIsReady && (this.DashTimer == null || this.DashTimer.IsElapsed); }
    }

    public bool IsDashing {
        get { return this.DashTimer != null && this.DashTimer.IsRunning; }
    }

    public DashingBehaviour() {
        this.Motion = new BasicMotion();
    }

    public override void OnInitialize() {
        this.DashTimer = new Timer(this.DashTime, this.Entity, trigger: () => {
            this.Body.Velocity = Vector2.zero;
            this.Body.EnableGravity = true;
        });
    }

    public override void OnUpdate() {
        if (this.IsDashing) {
            if (this.Entity.FrontIsOnWall) {
                StopDashing();
            }
        }
    }

    public void EnableDashing() {
        this.DashIsReady = true;
        this.DashTimer?.End();
    }

    public void StopDashing() {
        this.DashTimer.End();
    }

    public void Dash(Vector2 direction, float multiplier = 1f) {
        if (!this.CanDash) {
            return;
        }
        
        this.DashTimer.Duration = this.DashTime;
        this.Puppet.Do(this.Motion);
        
        var velocity = direction.normalized * this.DashSpeed * multiplier;
        this.Body.Velocity = velocity.Change(y: velocity.y * this.VerticalSpeedMultiplier);
        this.Body.EnableGravity = false; 
        
        this.DashIsReady = false;
        this.DashTimer.Reset();
    }
}

public class FlyingBehaviour : EntityBehaviour {
    [OdinSerialize] public float FlySpeed { get; set; } 
    [OdinSerialize] public float FlyAcceleration { get; set; } 
    [OdinSerialize] public float FlyDeceleration { get; set; } 
    [OdinSerialize, Required] public Motion IdleMotion { get; private set; } = new BasicMotion();
    [OdinSerialize, Required] public Motion FlyMotion { get; private set; } = new BasicMotion();

    public void Idle(Entity entity) {
        this.Puppet.Do(this.IdleMotion);
        this.Body.Velocity = Vector2.MoveTowards(this.Body.Position, Vector2.zero, this.FlyDeceleration);
    }

    public void Fly(Vector2 direction) {
        this.Puppet.Do(this.FlyMotion);
        this.Body.Velocity += direction * this.FlyAcceleration * Timing.Delta;
        this.Body.Velocity = Vector2.ClampMagnitude(this.Body.Velocity, this.FlySpeed);
    } 
}


public class EntityEmission : IPositionEditable {
    [OdinSerialize, Required] Entity EntityPrefab { get; set; }
    [OdinSerialize] public float EmittedTime { get; private set; }
    [OdinSerialize] Vector2 Position { get; set; }
    [OdinSerialize] Vector2 Direction { get; set; } = Vector2.right;

    public bool WasEmitted { get; private set; }

    public Vector2 EditablePosition {
        get { return this.Position; }
        set { this.Position = value; }
    }

    public ProjectileBehaviour Emit(Vector2 origin, HorizontalDirection facing = HorizontalDirection.Right, IProjectileOwner owner = null) {
        return Emit(origin, this.Direction, facing, owner);
    }

    public ProjectileBehaviour Emit(Vector2 origin, Vector2 direction, HorizontalDirection facing = HorizontalDirection.Right, IProjectileOwner owner = null) {
        var entity = this.EntityPrefab.CreatePooled();
        var projectile = entity.GetBehaviour<ProjectileBehaviour>();
        var position = this.Position;

        if (facing == HorizontalDirection.Left) {
            position.x = -position.x;
            direction.x = -direction.x;
        }

        if (projectile != null) {
            projectile.Fire(origin + position, direction, owner: owner);
        } else {
            entity.Puppet.Body.Position = position;
            entity.Puppet.Facing = facing;
        }
        
        this.WasEmitted = true;
        
        return projectile;
    }

    public void Reset() {
        this.WasEmitted = false;
    }

#if UNITY_EDITOR
    [Button]
    public void Edit() {
        PositionTool.BeginEdit(this);
    }
#endif
}

public class ProjectileBehaviour : EntityBehaviour {
    const float MinRotateToVelocity = 0.01f;
    
    [OdinSerialize] public GameObjectFlag RequiredFlagsToHit { get; private set; } = GameObjectFlag.Player;

    [OdinSerialize] public HitInfo HitInfo { get; private set; } = new HitInfo();
    [OdinSerialize] public float Speed { get; private set; }
    [OdinSerialize] public float Acceleration { get; private set; }
    [OdinSerialize] public float MaxTimeAlive { get; private set; }
    
    [OdinSerialize, Required] public Motion FlyMotion { get; private set; } = new BasicMotion();
    [OdinSerialize, Required] public Motion DetonateMotion { get; private set; } = new BasicMotion();
    
    [OdinSerialize] public bool SticksOnHit { get; private set; }

    public GameObject Target { get; private set; }
    
    public bool HasDetonated { get; private set; }
    
    Timer DestroyTimer { get; set; }
    IProjectileOwner Owner { get; set; }
    
    public override void OnInitialize() {
        this.Body.Collider.isTrigger = true;
        this.DestroyTimer = new Timer(this.MaxTimeAlive, this.Entity);
    }

    public override void OnEnable() {
        this.Body.Collider.enabled = true;
        this.HasDetonated = false;
        this.DestroyTimer.Reset();
    }

    public override void OnDisable() {
        this.Body.Collider.enabled = false;
        this.HasDetonated = false;
        this.Owner = null;
        this.DestroyTimer.Reset();
    }
    
    public override void OnUpdate() {
        if (this.HasDetonated) {
            if (this.Puppet.CurrentMotionHasFinished) {
                Destroy();
            }
            return;
        }
        
        if (this.DestroyTimer.IsElapsed) {
            Detonate();
            return;
        } 
        
        this.Puppet.Do(this.FlyMotion);
        
        RotateToVelocity();
    }

    public override void OnTriggerEnter(Collider2D collider) {
        var obj = collider.gameObject;
        var hit = false;

        if (obj.HasFlag(GameObjectFlag.Obstacle)) {
            Detonate();
            hit = true;
        } 
        
        var entity = obj.GetComponent<Entity>();
        
        if (entity != null && entity.Info.HasFlag(this.RequiredFlagsToHit)) {
            entity.OnHit(this.HitInfo.ToHit((entity.Puppet.Body.Position - this.Body.Position).normalized));
            Detonate();
            this.Owner?.OnProjectileHit(this, entity.gameObject);
            hit = true;
        }

        if (hit) {
            if (this.SticksOnHit) {
                this.Entity.transform.parent = obj.transform;
            }
        }
    }
    
    public void Detonate() {
        if (this.HasDetonated) {
            return;
        }
        
        this.Puppet.Do(this.DetonateMotion);
        
        this.Body.Velocity = Vector2.zero;
        this.Body.Rotation = 0f;
        this.Body.Collider.enabled = false;
        this.HasDetonated = true;

        this.Owner?.OnProjectileDetonate(this);
    }
    
    public void Fire(Vector2 direction, GameObject target = null, IProjectileOwner owner = null) {
        this.Target = target;
        this.DestroyTimer.Reset();
        this.Owner = owner;
        
        this.Body.Velocity = this.Speed * direction;
        this.Puppet.Do(this.FlyMotion);
        
        RotateToVelocity();

        this.Owner?.OnProjectileFired(this, direction);
    }

    public void Fire(Vector2 position, Vector2 direction, GameObject target = null, IProjectileOwner owner = null) {
        Move(position);
        Fire(direction, target, owner);
    }
    
    public void Move(Vector2 position) {
        this.Body.Position = position;
    }

    public void Destroy() {
        Game.Instance.Pooler.Return(this.Entity.gameObject);
    }
    
    void RotateToVelocity() {
        if (this.Body.Velocity.sqrMagnitude > MinRotateToVelocity.Squared()) {
            this.Body.Rotation = this.Body.Velocity.ToFloatRotation();
        }
    }
}

public class ToggleBehaviour : EntityBehaviour {
    [OdinSerialize, Required] public Motion IdleOnMotion { get; set; } = new BasicMotion();
    [OdinSerialize, Required] public Motion IdleOffMotion { get; set; } = new BasicMotion();
    [OdinSerialize, Required] public Motion TurnOnMotion { get; set; } = new BasicMotion();
    [OdinSerialize, Required] public Motion TurnOffMotion { get; set; } = new BasicMotion();
    
    [OdinSerialize] string WorldStateVariable { get; set; }
    
    public bool Transitioning { get; private set; }

    public bool On { get; private set; }
    
    bool TemporaryOn { get; set; }

    public void Set(bool on) {
        this.On = on;
        Idle();
    }

    public bool Toggle() {
        if (!this.Transitioning) {
            this.Entity.Puppet.Do(this.On ? this.TurnOffMotion : this.TurnOnMotion);
            this.Transitioning = true;
        }
        return this.On;
    }

    public void Idle() {
        this.Entity.Puppet.Do(this.On ? this.IdleOnMotion : this.IdleOffMotion);
    }

    public override void OnUpdate() {
        if (this.Puppet.CurrentMotion == this.TurnOnMotion || this.Puppet.CurrentMotion == this.TurnOffMotion) {
            if (this.Entity.Puppet.CurrentMotionHasFinished) {
                this.Transitioning = false;
            }
        }
    }
}
