﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Sirenix.Utilities;
using UnityEngine;
using Utilities;

[RequireComponent(typeof(Puppet))]
[RequireComponent(typeof(ObjectInfo))]
public abstract class Entity : SerializedMonoBehaviour, IProjectileOwner {
    
    public const float AIDelay = 0.25f;
    
    static readonly Dictionary<Type, MemberInfo[]> BehaviourMemberInfos = new Dictionary<Type, MemberInfo[]>();
    
    [OdinSerialize] 
    public bool IsInvulnerable { get; set; }
    [OdinSerialize, Required] 
    public Resource Health { get; private set; } = new Resource();
    
    public Puppet Puppet { get; set; }
    
    [OdinSerialize, Required] 
    protected AnimationClip IdleAnimation { get; set; }
    protected bool IsDying { get; set; }
    
    protected bool UseAimRotation { get; set; }
    public Vector2 AimDirection { get; set; }

    public ObjectInfo Info => this.info ?? (this.info = GetComponent<ObjectInfo>()); 
    
    public bool IsGrounded => this.Puppet.IsGrounded;
    public bool IsInAir => !this.Puppet.IsGrounded;
    public bool IsOnWall => this.IsInAir && this.Puppet.IsTouchingWall;
    public bool FrontIsOnWall => this.IsInAir && this.Puppet.FrontIsTouchingWall;
    public bool BackIsOnWall => this.IsInAir && this.Puppet.BackIsTouchingWall;
    public bool IsOnRoof => this.IsInAir && this.Puppet.IsTouchingRoof;
    
    Action<Entity> SustainedAction { get; set; }
    List<EntityBehaviour> Behaviours { get; } = new List<EntityBehaviour>();
    
    ObjectInfo info;
    Timer AITimer { get; set; }
    
    void Awake() {
        this.Puppet = GetComponent<Puppet>();
        this.Puppet.ProjectileOwner = this;
        
        this.AITimer = new Timer(AIDelay, this, loop: true, trigger: RunAI);
        this.AITimer.RandomizeTimeRemaining();
        
        RegisterAllBehaviours();
        OnInitialize();
        
        for (var i = 0; i < this.Behaviours.Count; i++) {
            this.Behaviours[i].OnInitialize();
        }
    }

    void OnEnable() {
        for (var i = 0; i < this.Behaviours.Count; i++) {
            this.Behaviours[i].OnEnable();
        }
    }

    void OnDisable() {
        for (var i = 0; i < this.Behaviours.Count; i++) {
            this.Behaviours[i].OnDisable();
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        for (var i = 0; i < this.Behaviours.Count; i++) {
            this.Behaviours[i].OnTriggerEnter(collider);
        }
    }

    void Update() {
        if (this.Puppet.CurrentMotion != null) {
            OnAnimate(this.Puppet.CurrentMotion);
        }
    }

    void FixedUpdate() {
        if (this.IsDying) {
            if (this.Puppet.CurrentMotionHasFinished) {
                DestroyImmediate(this.gameObject);
            }
            return;
        }
        
        OnUpdate();

        for (var i = 0; i < this.Behaviours.Count; i++) {
            this.Behaviours[i].OnUpdate();
        }

        this.SustainedAction?.Invoke(this);

        HandleMotion(this.Puppet.CurrentMotion);
    }

    void HandleMotion(Motion motion) {
        if (motion is AdvancedMotion) {
            var advanced = (AdvancedMotion) motion;
            var position = this.Puppet.Body.Position;
            var facing = this.Puppet.Facing;

            foreach (var zone in advanced.ActionZones) {
                if (this.Puppet.TriggerZoneIsActive(zone)) {
                    var collisions = zone.TriggerZone.DetectTriggers(position, facing);
                    for (var i = 0; i < collisions.Length; i++) {
                        OnActionZone(zone.ID, collisions[i].gameObject);
                    }
                }
            }

            foreach (var emission in advanced.EntityEmissions) {
                if (this.Puppet.EmissionIsActive(emission)) {
                    if (this.UseAimRotation) {
                        emission.Emit(position, this.AimDirection, facing, this);
                    } else {
                        emission.Emit(position, facing, this);
                    }
                }
            }
        }
    }

    void RunAI() {
        this.SustainedAction = null;
        OnAI();
    }

    protected virtual void OnInitialize() { }
    protected virtual void OnUpdate() { }
    protected virtual void OnAI() { }
    protected abstract void OnAnimate(Motion motion); 
    public virtual void OnActionZone(string id, GameObject obj) { }
    public virtual void OnProjectileDetonate(ProjectileBehaviour projectile) { }
    public virtual void OnProjectileHit(ProjectileBehaviour projectile, GameObject obj) { }
    public virtual void OnProjectileFired(ProjectileBehaviour projectile, Vector2 direction) { }
    
    public virtual void OnHit(Hit hit) {
        var behaviour = GetBehaviour<HitBehaviour>();
        if (behaviour != null) {
            behaviour.OnHit(hit);
            if (this.Health.IsDepleted) {
                Die();
            }
        }
    }

    public virtual void Die() {
        if (this.IsDying) {
            return;
        }

        var behaviour = GetBehaviour<DeathBehaviour>();
        if (behaviour != null) {
            behaviour.Die();
        } else {
            Destroy(this.gameObject);
            return;
        }

        this.IsDying = true;
    }

    public void RegisterBehaviour([CanBeNull] EntityBehaviour behaviour) {
        if (behaviour == null) {
            return;
        }
        this.Behaviours.AddIfUnique(behaviour);
        behaviour.LinkTo(this);
    }

    public T GetBehaviour<T>() where T : EntityBehaviour {
        for (var i = 0; i < this.Behaviours.Count; i++) {
            var behaviour = this.Behaviours[i];
            if (behaviour is T) {
                return (T) behaviour;
            }
        }
        return null;
    }
    
    public Entity CreatePooled() {
        return Game.Instance.Pooler.Get(this.gameObject).GetComponent<Entity>();
    }

    protected void SustainAction(Action<Entity> action) {
        this.SustainedAction = action;
    }

    protected void ClearSustainedAction() {
        this.SustainedAction = null;
    }

    protected bool ChancePerSecond(float chance) {
        return Randomize.Chance(chance * AIDelay);
    }
    
    protected bool ChancePerMinute(float chance) {
        return Randomize.Chance(chance * (AIDelay / 60));
    }

    void RegisterAllBehaviours() {
        var type = GetType();
        var members = BehaviourMemberInfos.GetOrNull(type);
        if (members == null) {
            members = type.GetFieldsAndPropertiesOfType<EntityBehaviour>().ToArray();
            BehaviourMemberInfos[type] = members;
        }
        foreach (var member in members) {
            RegisterBehaviour((EntityBehaviour) member.GetMemberValue(this));
        }
    }
}
