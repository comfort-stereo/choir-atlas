﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class BasicProjectile : Entity {
    [OdinSerialize, Required] AnimationClip DetonateMotion { get; set; }
    [OdinSerialize, Required] public ProjectileBehaviour ProjectileBehaviour { get; private set; } = new ProjectileBehaviour();

    protected override void OnAnimate(Motion motion) {
        this.Puppet.Link(this.ProjectileBehaviour.FlyMotion, this.IdleAnimation);
        this.Puppet.Link(this.ProjectileBehaviour.DetonateMotion, this.DetonateMotion);
    }
}
