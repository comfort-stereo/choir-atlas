using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class Character : SerializedMonoBehaviour {
    
    [OdinSerialize, Required] CharacterData Data { get; set; }

    public static Character Create(CharacterData data) {
        var instance = new GameObject(data.Name, typeof(Character)).GetComponent<Character>();
        instance.Data = data;
        return instance;
    }
}
