﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Utilities;

public class RotatingObstacle : SerializedMonoBehaviour {
    
    [OdinSerialize] public float RotationSpeed { get; set; }

    void Update() {
        var rotation = this.transform.eulerAngles;
        this.transform.eulerAngles = rotation.Change(z: rotation.z + this.RotationSpeed * Timing.Delta);
    }
}
