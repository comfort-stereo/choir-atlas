﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Utilities;

public class MovingObstacle : SerializedMonoBehaviour {
    
    [OdinSerialize] Vector2 PatrolOffset { get; set; }
    [OdinSerialize] float Speed { get; set; }
    
    Vector2 StartPosition { get; set; }
    
    bool GoingAway { get; set; }

    void Awake() {
        this.StartPosition = this.transform.position;
        this.GoingAway = true;
    }

    void FixedUpdate() {
        var destination = this.GoingAway ? this.StartPosition + this.PatrolOffset : this.StartPosition;
        
        this.transform.position = Vector2.MoveTowards(this.transform.position, destination, this.Speed * Timing.Delta);
        
        if (this.transform.position.To2() == destination) {
            this.GoingAway = !this.GoingAway;
        }
    }
}
