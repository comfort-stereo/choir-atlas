﻿using ChoirUtilities;
using UnityEngine;
using Utilities;

public class CameraController : MonoBehaviour {

    const float CullingDelay = 0.1f;
    const float GameObjectCullingDistance = 6f;
    const float ObstacleCullingDistance = 10f;
    const float DecorativeCullingDistance = 4.25f;
    const float Speed = 7f;
    const float DistanceToSpeedMultiplier = 3f;
    const float FacingOffsetDistance = 0f;
    const float AnalogPositionOffset = 2f;
    const float YImmediateFollowDistance = 1f;

    Atlas Player { get; set; }
    Timer GameObjectCullingTimer { get; set; }

    void Awake() {
        this.GameObjectCullingTimer = new Timer(CullingDelay, this, loop: true, trigger: CullGameObjects);
        var data = PoolingData.BuildFromCurrentScene();
        Game.Instance.Pooler.Provide(data);
        Application.targetFrameRate = 120;
    }

    void Update() {
        this.Player = this.Player ?? (this.Player = FindObjectOfType<Atlas>());
        
        if (this.Player == null) return;

        var analog = Game.Instance.InputManager.AnalogRight * AnalogPositionOffset;
        var facing = this.Player.Puppet.Facing.ToVector() * FacingOffsetDistance;
        var target = this.Player.transform.position.To2() + analog + facing; 
        var distance = Vector2.Distance(this.transform.position.To2(), target);
        var delta = (Speed + DistanceToSpeedMultiplier * distance) * Timing.Delta;
        
        this.transform.position = Vector3.MoveTowards(this.transform.position, target, delta).Change(z: -1);

        var y = Mathf.Abs(this.transform.position.y - target.y);
        
        if (y > YImmediateFollowDistance) {
            if (this.transform.position.y > target.y) {
                this.transform.position = this.transform.position.Change(y: target.y + YImmediateFollowDistance);
            } else {
                this.transform.position = this.transform.position.Change(y: target.y - YImmediateFollowDistance);
            }
        }
    }
    
    void CullGameObjects() {
        GameObjectUtilities.ApplyToAllGameObjects(CullGameObject);
        Game.Instance.Pooler.RedrawProvided(this.transform.position.To2(), ObstacleCullingDistance, DecorativeCullingDistance);
    }

    bool CullGameObject(GameObject obj) {
        var position = this.transform.position;
        var isObstacle = obj.HasFlag(GameObjectFlag.Obstacle);

        if (isObstacle || obj.GetComponent<Entity>() != null) {
            Vector2 point;
            
            var collider = obj.GetComponent<Collider2D>();
            if (collider == null) {
                point = obj.transform.position;
            } else {
                point = collider.bounds.ClosestPoint(position);
            }

            var cullingDistance = isObstacle ? ObstacleCullingDistance : GameObjectCullingDistance;
            obj.SetActive(position.To2().SqrDistance(point) < cullingDistance.Squared());
            return false;
        }

        return true;
    }
}
