#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

public class ChoirAssetPostprocessor : AssetPostprocessor {

    TextureImporter TextureImporter {
        get { return (TextureImporter) this.assetImporter; }
    }
    
    void OnPreprocessTexture() {
        this.TextureImporter.spritePixelsPerUnit = 100;
        this.TextureImporter.filterMode = FilterMode.Point;
        this.TextureImporter.textureCompression = TextureImporterCompression.Uncompressed;
        this.TextureImporter.spriteImportMode = SpriteImportMode.Multiple;
    }
}

#endif
