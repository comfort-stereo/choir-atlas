﻿using UnityEngine;

namespace ChoirUtilities {
    using JetBrains.Annotations;
    using Utilities;

    public static class Layers {
        public const int Default = 0;
        public const int IgnoreRaycast = 2;
        public const int UI = 5;
    }

    public static class LayerMasks {
        public const int All = int.MaxValue & ~(1 << Layers.IgnoreRaycast);
        public const int None = 0;
        public const int Standard = -5;
    }

    public static class GameObjectExtensions {
        
        static SimpleObjectInfo NullObjectInfo { get; } = new SimpleObjectInfo();

        public static PhysicsBody AddPhysicsBody(this GameObject obj, bool gravity = true, bool rotation = false) {
            var body = obj.AddOrReplaceComponent<PhysicsBody>();
            body.FreezeRotation = !rotation;
            body.EnableGravity = gravity;
            return body;
        }

        public static AnimationBit AddChildedAnimationBit(this GameObject obj, string name, string layer) {
            var child = obj.AddChild(new GameObject(name));
            var bit = child.AddAnimationBit(layer);
            return bit;
        }
        
        public static AnimationBit AddAnimationBit(this GameObject obj, string layer = "Default") {
            var controller = obj.AddOrReplaceComponent<AnimationBit>();
            var renderer = obj.GetComponent<SpriteRenderer>();
            if (renderer == null) {
                renderer = obj.AddComponent<SpriteRenderer>();
            }
            renderer.sortingLayerName = layer;
            return controller;
        }

        [NotNull]
        public static IObjectInfo GetInfo(this GameObject obj) {
            return obj.GetComponent<IObjectInfo>() ?? NullObjectInfo;
        }

        public static bool HasFlag(this GameObject obj, GameObjectFlag flag) {
            return obj.GetInfo().HasFlag(flag);
        }

        public static bool HasTag(this GameObject obj, string tag) {
            return obj.GetInfo().HasTag(tag);
        }

        [CanBeNull]
        public static ObjectInfo GetInfoComponent(this GameObject obj) {
            return obj.GetComponent<ObjectInfo>();
        }
    }
}
