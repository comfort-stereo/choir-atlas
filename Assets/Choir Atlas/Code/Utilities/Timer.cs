﻿using UnityEngine;
using Utilities;

public enum TimerUpdateType {
    Normal,
    True
}

public class Timer {

    public event Callback OnElapsed;

    public bool IsLooping { get; set; }
    public float Remaining { get; set; }
    public bool IsDestroyed { get; private set; }
    public Component Owner { get; private set; }
    public TimerUpdateType UpdateType { get; private set; }
    
    bool Triggered { get; set; }
    bool DestroyWithOwner { get; set; }

    public bool IsRunning {
        get { return this.Remaining > 0f; }
    }

    public bool IsElapsed {
        get { return this.Remaining == 0f; }
    }
    
    public float Duration {
        get { return this.duration; }
        set { this.duration = Mathf.Max(0f, value); }
    } float duration;
    
    public Timer(float duration, Component owner = null, bool loop = false, Callback trigger = null, 
        TimerUpdateType updateType = TimerUpdateType.Normal) 
    {
        this.Duration = duration;
        this.IsLooping = loop;
        this.Owner = owner; 
        this.DestroyWithOwner = this.Owner != null;
        this.OnElapsed = trigger;
        this.UpdateType = updateType;
        
        if (this.IsLooping) {
            Reset();
        }
        
        UpdateListener.Instance.RegisterTimer(this);
    }

    public void Reset() {
        this.Remaining = this.Duration;
        this.Triggered = false;
    }

    public void End() {
        this.Remaining = 0f;
    }
    
    public void ResetTo(float duration) {
        this.Duration = duration;
        this.Remaining = this.Duration;
        this.Triggered = false;
    }

    public void Update(float delta) {
        if (this.DestroyWithOwner && this.Owner == null) {
            this.IsDestroyed = true;
            return;
        }

        if (this.Owner == null) {
            if (this.DestroyWithOwner) {
                this.IsDestroyed = true;
                return;
            }
        } else if (!this.Owner.gameObject.activeSelf) {
            return;
        }
        
        if (this.IsElapsed && !this.Triggered) {
            this.Triggered = true;
            if (this.OnElapsed != null) {
                this.OnElapsed();
            }
            if (this.IsLooping) {
                Reset();
            } 
        }
        this.Remaining = Mathf.Max(0f, this.Remaining - delta);
    }

    public void Destroy() {
        this.IsDestroyed = true;
    }

    public void RandomizeTimeRemaining() {
        this.Remaining = Randomize.Value * this.Duration;
    }
}
