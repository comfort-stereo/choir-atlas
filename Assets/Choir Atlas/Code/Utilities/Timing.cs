﻿using UnityEngine;

public static class Timing {
    
    public static bool Paused { get; set; }
    
    public static float Delta {
        get { return Paused ? 0f : Time.deltaTime; }
    }

    public static float TrueDelta {
        get { return Time.deltaTime; }
    }

    static Timing() {
        Paused = false;
    }
}
