using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Utilities {
    using System.Reflection;
    using Sirenix.Utilities;
    using UnityEngine.Assertions;

    public delegate void Callback();

    public static class Randomize {
        
        public static bool Chance(float ratio) {
            return Value < ratio;
        }

        public static float Value {
            get { return UnityEngine.Random.value; }
        }
    }
    
    public static class Array<T> {
        public static readonly T[] Empty = new T[0];
    }

    public static class TimeUtility {
        
        public static float FPS {
            get { return 1f / Time.deltaTime; }
        }
    }

    public static class GizmoUtility {

        static Color? PreviousColor;
        
        public static void StartColor(Color color) {
            PreviousColor = Gizmos.color;
            Gizmos.color = color;
        }

        public static void EndColor() {
            Gizmos.color = PreviousColor.GetValueOrDefault();
        }
    }

    public static class ReflectionUtilities {
        
        public static IEnumerable<MemberInfo> GetFieldsAndPropertiesOfType<T>(this Type type) {
            var flags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            foreach (var field in type.GetFields(flags)) {
                if (field.FieldType.IsSubclassOf(typeof(T))) {
                    yield return field;
                }
            }
            foreach (var property in type.GetProperties(flags)) {
                if (property.PropertyType.IsSubclassOf(typeof(T))) {
                    yield return property;
                }
            }
        }
    }

    public static class ContainerExtensions {
        
        public static void AddIfExists<T>(this List<T> list, T element) where T : class {
            if (element != null) {
                list.Add(element);
            }
        }

        public static bool AddIfUnique<T>(this List<T> list, T element) {
            for (var i = 0; i < list.Count; i++) {
                if (Equals(list[i], element)) {
                    return false;
                }
            }
            list.Add(element);
            return true;
        }

        public static int OccurrencesOf<T>(this List<T> list, T element) where T : class {
            var count = 0;
            for (var i = 0; i < list.Count; i++) {
                if (list[i] == element) {
                    count++;
                }
            }
            return count;
        }

        public static V GetOrNull<K, V>(this Dictionary<K, V> dictionary, K key) where V : class {
            V value; 
            dictionary.TryGetValue(key, out value);
            return value;
        }
        
        public static T GetOrNull<T>(this List<T> list, int index) where T : class {
            if (index < 0 || index > list.Count - 1) {
                return null;
            }
            return list[index];
        }

        public static T GetOrNull<T>(this T[] array, int index) where T : class {
            if (index < 0 || index > array.Length - 1) {
                return null;
            }
            return array[index];
        }
    }

    namespace Containers {
        using JetBrains.Annotations;
        using Sirenix.Serialization;

        public class Box<T> {

            [OdinSerialize] public T Value { get; private set; }

            public Box([NotNull] T value) {
                this.Value = value;
            }
        }
        
        public struct ArraySlice<T> {
            public static readonly ArraySlice<T> Empty = new ArraySlice<T>(Array<T>.Empty, 0, 0);
            
            ArraySegment<T> segment;

            public ArraySlice(T[] array, int offset, int count) {
                this.segment = new ArraySegment<T>(array, offset, count);
            }
            public int Length {
                get { return this.segment.Count; }
            }

            public T this[int index] {
                get { return this.segment.Array[this.segment.Offset + index]; }
            }

            public T[] ToArray() {
                var temp = new T[this.segment.Count];
                Array.Copy(this.segment.Array, this.segment.Offset, temp, 0, this.segment.Count);
                return temp;
            }
        } 
        
        public struct Tuple<T1, T2> {
            T1 first;
            T2 second;

            public Tuple(T1 first, T2 second) {
                this.first = first;
                this.second = second;
            }

            public T1 First {
                get { return this.first; }
            }

            public T2 Second {
                get { return this.second; }
            }

            public override int GetHashCode() {
                unchecked {
                    var result = 37;
                    
                    result *= 23;
                    if (this.First != null) {
                        result += this.First.GetHashCode();
                    }

                    result *= 23;
                    if (this.Second != null) {
                        result += this.Second.GetHashCode();
                    }

                    return result;
                }
            }

            public override bool Equals(object obj) {
                if (obj == null) return false;
                if (obj.GetType() != typeof(Tuple<T1, T2>)) return false;

                var other = (Tuple<T1, T2>) obj;
                return Equals(other.First, this.First) && Equals(other.Second, this.Second);
            }
        }
    }
    
    public static class GameObjectUtilities {

        public static GameObject AddChild(this GameObject obj, GameObject child) {
            obj.Attach(child);
            return child;
        }

        public static T AddOrReplaceComponent<T>(this GameObject obj) where T : Component {
            var component = obj.GetComponent<T>();
            if (component != null) {
                Object.DestroyImmediate(component);
            }
            component = obj.gameObject.AddComponent<T>();
            return component;
        }

        public static GameObject Copy(this GameObject obj, string name = null) {
            var copy = Object.Instantiate(obj);
            copy.name = name ?? obj.name;
            return copy;
        }

        public static GameObject Find(this GameObject obj, string name) {
            var transform = obj.transform.Find(name);
            if (transform != null) {
                return transform.gameObject;
            }
            return null;
        }
        
        public static GameObject Find(this MonoBehaviour obj, string name) {
            return Find(obj.gameObject, name);
        }

        public static GameObject Get(this GameObject obj, string name) {
            var child = Find(obj, name);
            Assert.IsNotNull(child, "Attempted to get child with name '" + name + "' from '" + obj.name + "' but it did not exist.");
            return child;
        }
        
        public static GameObject Get(this MonoBehaviour obj, string name) {
            return obj.gameObject.Get(name);
        }
        
        public static GameObject FindBy(this GameObject obj, Predicate<GameObject> predicate) {
            var transform = obj.transform;
            var count = transform.childCount;
            for (var i = 0; i < count; i++) {
                var child = transform.GetChild(i).gameObject;
                if (predicate(child)) {
                    return child;
                }
                var subchild = child.FindBy(predicate);
                if (subchild != null) {
                    return subchild;
                }
            }
            return null;
        }

        public static GameObject GetBy(this GameObject obj, Predicate<GameObject> predicate) {
            var child = obj.FindBy(predicate);
            Assert.IsNotNull(child, "Attempted to get child by predicate from '" + obj.name + "' but it did not exist.");
            return child;
        }

        public static GameObject FindTag(this GameObject obj, string tag) {
            return obj.FindBy(child => child.CompareTag(tag));
        }
        
        public static GameObject GetTag(this GameObject obj, string tag) {
            var child = obj.FindTag(tag);
            Assert.IsNotNull(child, "Attempted to get child with tag '" + tag + "' from '" + obj.name + "' but it did not exist.");
            return child;
        }

        public static void Attach(this GameObject obj, GameObject child) {
            child.transform.parent = obj.transform;
            child.transform.position = obj.transform.position;
            child.transform.rotation = obj.transform.rotation;
        }

        public static void DestroyAndReplaceChildren(this GameObject obj, GameObject child) {
            obj.DestroyChildren();
            obj.Attach(child);
        }

        public static void DestroyChildren(this GameObject obj) {
            ApplyToAllGameObjectsInHierarchyDepthFirst(obj, Object.Destroy);
        }
        
        static List<GameObject> RootGameObjectBuffer { get; } = new List<GameObject>();
        
        public static void ApplyToAllGameObjects(Predicate<GameObject> action) {
            var scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
            scene.GetRootGameObjects(RootGameObjectBuffer);
            foreach (var obj in RootGameObjectBuffer) {
                ApplyToAllGameObjectsInHierarchy(obj, action);
            }
            
        }
        
        public static void ApplyToAllGameObjectsInHierarchy(this GameObject obj, Predicate<GameObject> action) {
            var transform = obj.transform;
            var count = transform.childCount;
            var descend = action(obj);
            if (descend) {
                for (var i = 0; i < count; i++) {
                    ApplyToAllGameObjectsInHierarchy(transform.GetChild(i).gameObject, action);
                }
            }
        }
        
        public static void ApplyToAllGameObjectsInHierarchyDepthFirst(this GameObject obj, Action<GameObject> action) {
            var transform = obj.transform;
            var count = transform.childCount;
            for (var i = 0; i < count; i++) {
                ApplyToAllGameObjectsInHierarchy(transform.GetChild(i).gameObject, child => {
                    action(child);
                    return true;
                });
            }
            action(obj);
        }
        
        public static void ApplyToAllGameObjectsInChildren(GameObject obj, Action<GameObject> action) {
            var transform = obj.transform;
            var count = transform.childCount;
            
            for (var i = 0; i < count; i++) {
                action(transform.GetChild(i).gameObject);
            }
        }

        public static GameObject[] FindGameObjectsWhere(Predicate<GameObject> predicate) {
            var found = new List<GameObject>();
            ApplyToAllGameObjects(obj => {
                if (predicate(obj)) {
                    found.Add(obj);
                }
                return true;
            });
            return found.ToArray();
        }

        public static GameObject FindGameObjectWhere(Predicate<GameObject> predicate) {
            var scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
            foreach (var obj in scene.GetRootGameObjects()) {
                var found = FindGameObjectInHierarchyWhere(obj, predicate);
                if (found != null) {
                    return found;
                }
            }
            return null;
        }

        static GameObject FindGameObjectInHierarchyWhere(GameObject obj, Predicate<GameObject> predicate) {
            if (predicate(obj)) {
                return obj;
            }
            var transform = obj.transform;
            var count = transform.childCount;
            
            for (var i = 0; i < count; i++) {
                var found = FindGameObjectInHierarchyWhere(transform.GetChild(i).gameObject, predicate);
                if (found != null) {
                    return found;
                }
            }
            return null;
        }
        
        public static bool GameObjectExistsWhere(Predicate<GameObject> predicate) {
            return FindGameObjectWhere(predicate) != null;
        }
    }
    
    public static class VectorExtensions {

        public static Vector2 ToNearestPixel(this Vector2 position) {
            return new Vector2(
                (float) Math.Round(position.x - 0.001f, 2), 
                (float) Math.Round(position.y - 0.001f, 2)
            );
        }

        public static Vector3 Change(this Vector3 vector, float x = float.MaxValue, float y = float.MaxValue, float z = float.MaxValue) {
            if (x != float.MaxValue) {
                vector.x = x;
            }
            if (y != float.MaxValue) {
                vector.y = y;
            }
            if (z != float.MaxValue) {
                vector.z = z;
            }
            return vector;
        }
        
        public static Vector2 Change(this Vector2 vector, float x = float.MaxValue, float y = float.MaxValue) { 
            if (x != float.MaxValue) {
                vector.x = x;
            }
            if (y != float.MaxValue) {
                vector.y = y;
            }
            return vector;
        } 
        
        public static Vector3 RotateAroundPivot(this Vector3 point, Vector3 pivot, Vector3 angles) {
            return Quaternion.Euler(angles) * (point - pivot) + pivot;
        } 
        
        public static Vector2 RotateAroundPivot(this Vector2 point, Vector2 pivot, float degrees) {
            return (Quaternion.Euler(0, 0, degrees) * (point - pivot)).To2() + pivot;
        } 
        
        public static Vector3 To3(this Vector2 vector) {
            return new Vector3(vector.x, vector.y, 0f);
        }
        
        public static Vector2 To2(this Vector3 vector) {
            return new Vector2(vector.x, vector.y);
        }

        public static Vector2 Rotate(this Vector2 vector, float degrees) {
            return Quaternion.Euler(0f, 0f, degrees) * vector; 
        }

        public static HorizontalDirection ToHorizontalDirection(this Vector2 vector) {
            if (vector.x == 0f) {
                return HorizontalDirection.None;
            }
            return vector.x > 0f ? HorizontalDirection.Right : HorizontalDirection.Left;
        }
        
        public static VerticalDirection ToVerticalDirection(this Vector2 vector) {
            if (vector.y == 0f) {
                return VerticalDirection.None;
            }
            return vector.y > 0f ? VerticalDirection.Up : VerticalDirection.Down;
        }

        public static Vector2 DirectionTo(this Vector2 vector, Vector2 other) {
            return (other - vector).normalized;
        }

        public static Vector2 Round(this Vector2 vector, int places = 0) {
            return new Vector2(
                (float) Math.Round(vector.x, places),
                (float) Math.Round(vector.y, places)
            );
        }
        
        public static Vector3 Round(this Vector3 vector, int places = 0) {
            return new Vector3(
                (float) Math.Round(vector.x, places),
                (float) Math.Round(vector.y, places),
                (float) Math.Round(vector.z, places)
            );
        }

        public static float ToFloatRotation(this Vector2 direction) {
            var value = (float)((Mathf.Atan2(direction.x, direction.y) / Math.PI) * 180f);
            return -value;
        }

        public static float SqrDistance(this Vector2 vector, Vector2 other) {
            return (vector - other).sqrMagnitude;
        }
    }

    public enum Direction {
        None, Up, Down, Left, Right
    }
    public enum HorizontalDirection {
        None, Left, Right
    }
    public enum VerticalDirection {
        None, Up, Down
    }

    public static class DirectionExtensions {
        
        public static Vector2 ToVector(this Direction direction) {
            switch (direction) {
                case Direction.None:
                    return Vector2.zero;
                case Direction.Up:
                    return Vector2.up;
                case Direction.Down:
                    return Vector2.down;
                case Direction.Left:
                    return Vector2.left;
                default:
                    return Vector2.right;
            }
        }
        
        public static Vector2 ToVector(this HorizontalDirection direction) {
            switch (direction) {
                case HorizontalDirection.Left:
                    return Vector2.left;
                case HorizontalDirection.Right:
                    return Vector2.right;
                default:
                    return Vector2.zero;
            }
        }
        
        public static Vector2 ToVector(this VerticalDirection direction) {
            switch (direction) {
                case VerticalDirection.Up:
                    return Vector2.left;
                case VerticalDirection.Down:
                    return Vector2.right;
                default:
                    return Vector2.zero;
            }
        }

        public static bool IsHorizontal(this Direction direction) {
            return direction == Direction.Left || direction == Direction.Right;
        }
        
        public static bool IsVertical(this Direction direction) {
            return direction == Direction.Up || direction == Direction.Down;
        }

        public static HorizontalDirection Opposite(this HorizontalDirection direction) {
            return direction == HorizontalDirection.Left ? HorizontalDirection.Right : HorizontalDirection.Left;
        }
        
        public static VerticalDirection Opposite(this VerticalDirection direction) {
            return direction == VerticalDirection.Up ? VerticalDirection.Down : VerticalDirection.Up;
        }
    }

    public static class MathExtensions {
        
        public static int Squared(this int number) {
            return number * number;
        }
        public static float Squared(this float number) {
            return number * number;
        }
    }

    #if UNITY_EDITOR 
    public static class ExtendedEditorUtility {
        
		public static Vector3 GetMouseWorldPosition() {
			var ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
			var plane = new Plane(Vector3.forward,Vector3.zero);
			
			var position = Vector3.zero;
			
			float distance;
			if (plane.Raycast(ray, out distance)) {
				position = ray.GetPoint(distance);
			}

			return position;
		}
    }
    #endif

    public static class AnyExtensions {
        public static T Apply<T>(this T obj, Action<T> action) where T : class {
            action(obj);
            return obj;
        }
        
        public static R ApplyAs<T, R>(this T obj, Action<R> action) where R : T {
            var casted = (R) obj;
            action(casted);
            return casted;
        }

        public static R Cast<T, R>(this T obj) where R : T {
            return (R) obj;
        }
    } 
}
