﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Utilities;

public class UpdateListener : SerializedMonoBehaviour {

    static UpdateListener Self { get; set; }

    List<Timer> Timers { get; } = new List<Timer>();
    
    public event Callback OnUpdate;
    public event Callback OnFixedUpdate;
    public event Callback OnGUIUpdate;

    public static UpdateListener Instance {
        get {
            if (Self == null) {
                Self = new GameObject("Update Listener", typeof(UpdateListener)).GetComponent<UpdateListener>();
                DontDestroyOnLoad(Self);
            } 
            return Self;
        }
    }

    public int TimerCount {
        get { return this.Timers.Count - this.Timers.OccurrencesOf(null); }
    }

    public void RegisterTimer(Timer timer) {
        var i = this.Timers.IndexOf(null);
        if (i < 0) {
            this.Timers.Add(timer);
        } else {
            this.Timers[i] = timer;
        }
    }
    
    void Update() {
        this.OnUpdate?.Invoke();
    }

    void FixedUpdate() {
        UpdateTimerList(this.Timers);
        this.OnFixedUpdate?.Invoke();
    }

    void OnGUI() {
        this.OnGUIUpdate?.Invoke();
    }

    void UpdateTimerList(List<Timer> timers) {
        for (var i = 0; i < timers.Count; i++) {
            var timer = timers[i];
            if (timer == null) continue;
            if (timer.IsDestroyed) {
                timers[i] = null;
            } else {
                if (timer.UpdateType == TimerUpdateType.Normal) {
                    timer.Update(Timing.Delta);
                } else {
                    timer.Update(Timing.TrueDelta);
                }
            }
        }
    }
}
