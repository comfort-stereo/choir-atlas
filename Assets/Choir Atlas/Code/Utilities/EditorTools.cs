﻿#if UNITY_EDITOR

using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using Utilities;

[InitializeOnLoad]
public class TriggerZoneTool {
    
    static TriggerZone TriggerZone { get; set; }
    static Vector2? StartPosition { get; set; }

    static TriggerZoneTool() {
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
        SceneView.onSceneGUIDelegate += OnSceneGUI;
    }
    
    [Button]
    public static void BeginEdit(TriggerZone zone) {
        Debug.Log("Collision Zone Edit Started");
        TriggerZone = zone;
    }

    public static void EndEdit() {
        if (TriggerZone != null) {
            Debug.Log("Collision Zone Edit Ended");
            TriggerZone = null;
            StartPosition = null;
        }
    }

    public static void OnSceneGUI(SceneView view) {
        var input = Event.current;
        var selected = Selection.activeGameObject;
        
        if (input.keyCode == KeyCode.Escape) {
            EndEdit();
            return;
        }

        if (TriggerZone == null) return;
        
        var origin = selected != null ? selected.transform.position : Vector3.zero; 

        if (input.isMouse && input.button == 1 && input.type == EventType.MouseDown) {
            if (StartPosition == null) {
                var start = (ExtendedEditorUtility.GetMouseWorldPosition() - origin).Round(2);
                StartPosition = start; 
                
                Debug.Log("Collision Zone Start Position: " + start);
            } else {
                var start = StartPosition.Value;
                var end = (ExtendedEditorUtility.GetMouseWorldPosition() - origin).Round(2);

                TriggerZone.SetBounds(start, end);
                
                Debug.Log("Collision Zone End Position: " + end);
                
                EndEdit();
            }
            input.Use();
        }
    }
}


[InitializeOnLoad]
public class PositionTool {
    
    static IPositionEditable Editable { get; set; }

    static PositionTool() {
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
        SceneView.onSceneGUIDelegate += OnSceneGUI;
    }
    
    [Button]
    public static void BeginEdit(IPositionEditable editable) {
        Debug.Log("Position Edit Started");
        Editable = editable;
    }

    public static void EndEdit() {
        Editable = null;
    }

    public static void OnSceneGUI(SceneView view) {
        var input = Event.current;
        var selected = Selection.activeGameObject;
        
        if (input.keyCode == KeyCode.Escape) {
            EndEdit();
            return;
        }

        if (Editable == null) return;

        var origin = selected != null ? selected.transform.position : Vector3.zero; 
        
        if (input.isMouse && input.button == 1 && input.type == EventType.MouseDown) {
            var position = (ExtendedEditorUtility.GetMouseWorldPosition() - origin).Round(2);
            Editable.EditablePosition = position;
            Debug.Log("Position: " + position);
            EndEdit();
            input.Use();
        }
    }
}

#endif

