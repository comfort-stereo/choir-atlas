﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ChoirUtilities;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Utilities;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

#if UNITY_EDITOR

using UnityEditor;

#endif

public struct Location {
    [OdinSerialize] public Vector2 Position;
    [OdinSerialize] public float Rotation;

    public Location(Vector2 position, float rotation) {
        this.Position = position;
        this.Rotation = rotation;
    }
}

public class PoolingDataEntry {
    [OdinSerialize] public GameObject Prefab { get; private set; }
    [OdinSerialize] public Location[] Locations { get; private set; }
    [OdinSerialize, ReadOnly] public int LocationCount { get; private set; }

    public PoolingDataEntry(GameObject prefab, Location[] locations) {
        this.Prefab = prefab;
        this.Locations = locations;
    }
}

public class PoolingData : SerializedMonoBehaviour {
    [OdinSerialize] public PoolingDataEntry[] Entries { get; private set; }

    #if UNITY_EDITOR
    
    public static PoolingData BuildFromCurrentScene() {
        var watch = new Stopwatch();
        var map = new Dictionary<GameObject, List<Location>>();
        
        watch.Start();

        GameObjectUtilities.ApplyToAllGameObjects(obj => {
            if (!obj.HasFlag(GameObjectFlag.Pooled)) {
                return true;
            }
            var prefab = (GameObject) PrefabUtility.GetPrefabParent(obj);
            if (prefab == null) {
                return true;
            }
            var locations = map.GetOrNull(prefab);
            if (locations == null) {
                locations = new List<Location>();
                map[prefab] = locations;
            }

            var transform = obj.transform;
            var location = new Location(transform.position.To2().ToNearestPixel(), transform.eulerAngles.z);
            
            locations.Add(location);
            
            Object.Destroy(obj);
            
            return false;
        });
        
        var data = new GameObject("Pooling Data", typeof(PoolingData)).GetComponent<PoolingData>();
        
        {
            var entries = new PoolingDataEntry[map.Count];
            var i = 0;
            foreach (var pair in map) {
                entries[i++] = new PoolingDataEntry(pair.Key, pair.Value.ToArray());
            }

            data.Entries = entries;
        }

        watch.Stop();
        
        Debug.Log("Built pooling data in " + watch.ElapsedMilliseconds + " milliseconds.");
        
        return data;
    }
    
    #endif
}

public class Pooler {
    
    class InstanceEntry {
        public Location Location { get; private set; }
        public GameObject Instance { get; set; }

        public InstanceEntry(Location location) {
            this.Location = location;
        }
    } 

    class Pool {
        public List<GameObject> Instances { get; private set; }
        public Transform Root { get; private set; }
        public InstanceEntry[] InstanceEntries { get; set; }
        public bool IsProvided { get; set; }

        public Pool(List<GameObject> instances, Transform root) {
            this.Instances = instances;
            this.Root = root;
            this.InstanceEntries = Array<InstanceEntry>.Empty;
        }
    }

    Dictionary<GameObject, Pool> Pools { get; set; }

    public Transform Root { get; private set; }

    public int PoolCount {
        get { return this.Pools.Count; }
    }

    public int PooledGameObjectCount {
        get {
            var count = 0;
            using (var iterator = this.Pools.GetEnumerator()) {
                while (iterator.MoveNext()) {
                    count += iterator.Current.Value.Instances.Count;
                }
            }
            return count;
        }
    }
    
    public Pooler() {
        this.Pools = new Dictionary<GameObject, Pool>();
        this.Root = new GameObject("Pooling System").transform;
        Object.DontDestroyOnLoad(this.Root);
    }

    public GameObject Get(GameObject prefab) {
        var pool = GetPool(prefab);
        var instance = GetInstanceFromPoolOrCreate(pool, prefab);
        instance.SetActive(true);
        
        return instance;
    }

    public void Return(GameObject instance) {
        instance.SetActive(false);
    }

    public void Clear() {
        using (var iterator = this.Pools.GetEnumerator()) {
            while (iterator.MoveNext()) {
                var pool = iterator.Current.Value;
                foreach (var obj in pool.Instances) {
                    Object.Destroy(obj);
                }
            }
        }
        this.Pools.Clear();
    }

    public void Provide(PoolingData data) {
        data.transform.parent = this.Root;
        foreach (var entry in data.Entries) {
            var pool = GetPool(entry.Prefab);
            pool.InstanceEntries = entry.Locations.Select(location => new InstanceEntry(location)).ToArray();
            pool.IsProvided = true;
        }
    }

    public void RedrawProvided(Vector2 center, float maxDistance, float maxDistanceDecorative) {
        
        foreach (var pair in this.Pools) {
            var prefab = pair.Key;
            var pool = pair.Value;

            if (!pool.IsProvided) continue;
            
            var collider = pool.Instances.FirstOrDefault()?.GetComponent<Collider2D>();
            var decorative = prefab.HasFlag(GameObjectFlag.Decorative);
            var bounds = collider != null ? collider.bounds : new Bounds();

            var maxDistanceSquared = (decorative ? maxDistanceDecorative : maxDistance).Squared();
            
            for (var i = 0; i < pool.InstanceEntries.Length; i++) {
                var entry = pool.InstanceEntries[i];
                var position = entry.Location.Position;
                
                float squareDistance;
                if (collider == null) {
                    squareDistance = (position - center).sqrMagnitude;
                } else {
                    bounds.center = position;
                    squareDistance = (bounds.ClosestPoint(center).To2() - center).sqrMagnitude;
                }
                
                if (squareDistance > maxDistanceSquared) {
                    if (entry.Instance != null) {
                        Return(entry.Instance);
                        entry.Instance = null;
                    }
                } else {
                    if (entry.Instance == null) {
                        entry.Instance = Get(prefab);
                        var transform = entry.Instance.transform;
                        transform.position = position;
                        transform.eulerAngles = new Vector3(0f, 0f, entry.Location.Rotation);
                    }
                }
            }
        }
    }

    GameObject GetInstanceFromPoolOrCreate(Pool pool, GameObject prefab) {
        
        for (var i = 0; i < pool.Instances.Count; i++) {
            var current = pool.Instances[i];
            if (current == null) {
                var copy = CopyPrefabAndInitialize(prefab, pool.Root);
                pool.Instances[i] = copy;
                return copy;
            }
            if (current.activeSelf) continue;
            return current;
        }
        
        var instance = CopyPrefabAndInitialize(prefab, pool.Root);
        
        pool.Instances.Add(instance);

        return instance;
    }

    Pool GetPool(GameObject prefab) {
        var current = this.Pools.GetOrNull(prefab);
        if (current != null) {
            return current;
        }
        
        var root = CreateSubRoot(prefab);
        var pool = new Pool(new List<GameObject>(), root);
        
        pool.Instances.Add(CopyPrefabAndInitialize(prefab, root));

        this.Pools[prefab] = pool;

        return pool;
    }

    Transform CreateSubRoot(GameObject prefab) {
        var root = new GameObject(prefab.name).transform;
        root.parent = this.Root;
        return root;
    }
    
    GameObject CopyPrefabAndInitialize(GameObject prefab, Transform root) {
        var copy = prefab.Copy();
        copy.SetActive(false);
        copy.transform.SetParent(root);
        return copy;
    }
}
