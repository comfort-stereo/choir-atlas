﻿using UnityEngine;
using Utilities;

public enum GameState {
    Menu,
    Play
}

public class Game {

	const string AssetReferenceTablePath = "Asset Reference Table";
	
    static readonly Rect DebugHUDArea = new Rect(0, 0, 200, 200);
	
	static Game Self { get; set; }
	
	public GameState State { get; private set; }
	public GameStateHandler Handler { get; private set; }
	public InputManager InputManager { get; private set; }
	public AssetReferenceTable AssetReferenceTable { get; private set; }
	public Pooler Pooler { get; private set; }
	public bool IsRunning { get; private set; }
	
	public static Game Instance {
		get { return Self ?? (Self = new Game()); }
	}

	Game() {
		this.InputManager = new InputManager();
		this.AssetReferenceTable = LoadAssetReferenceTable();
		this.Pooler = new Pooler();

		Application.targetFrameRate = -1;
	}
	
	public void Run(Startup startup) {
		if (this.IsRunning) return;
		
		UpdateListener.Instance.OnUpdate -= Update; 
		UpdateListener.Instance.OnUpdate += Update; 
		UpdateListener.Instance.OnFixedUpdate -= FixedUpdate; 
		UpdateListener.Instance.OnFixedUpdate += FixedUpdate; 
		UpdateListener.Instance.OnGUIUpdate -= GUIUpdate; 
		UpdateListener.Instance.OnGUIUpdate += GUIUpdate; 

		this.State = startup.State;

		if (this.State == GameState.Play) {
			this.Handler = new PlayStateHandler(startup.Save);
		} else {
			this.Handler = new MenuStateHandler();
		}

		this.IsRunning = true;
	}

	public void Update() {
		this.InputManager.Update();
	}

	public void FixedUpdate() {
		this.Handler.Update();
	}

	public void GUIUpdate() {
		DrawDebugHUD();
	}
	
    public void DrawDebugHUD() {
        GUILayout.BeginArea(DebugHUDArea);
	    GUILayout.Label("FPS");
        GUILayout.Label(TimeUtility.FPS.ToString());
        GUILayout.Label("Active Timers: " + UpdateListener.Instance.TimerCount);
        GUILayout.Label("Game Object Pools: " + this.Pooler.PoolCount);
        GUILayout.Label("Game Objects Pooled: " + this.Pooler.PooledGameObjectCount);
        GUILayout.EndArea();
    }

	AssetReferenceTable LoadAssetReferenceTable() {
		return (AssetReferenceTable) Resources.Load(AssetReferenceTablePath, typeof(AssetReferenceTable));
	}
}
