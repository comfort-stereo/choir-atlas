﻿
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

[CreateAssetMenu(fileName = "Stuff")]
public class AssetReferenceTable : SerializedScriptableObject {
    [OdinSerialize, Required] public AnimatorOverrideController DefaultAnimationOverrideController { get; private set; }
    [OdinSerialize, Required] public AnimationClip BlankAnimation { get; private set; }
}
