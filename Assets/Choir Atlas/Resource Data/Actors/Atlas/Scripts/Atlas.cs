﻿using System;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Utilities;

public class Atlas : Entity {
    const float JumpAddedAcceleration = 3f;
    
    [OdinSerialize, Required] AnimationClip RunAnimation { get; set; }
    [OdinSerialize, Required] AnimationClip InAirAnimation { get; set; }
    [OdinSerialize, Required] AnimationClip ShootingAnimation { get; set; }
    [OdinSerialize, Required] AnimationClip OnWallAnimation { get; set; }
    [OdinSerialize, Required] AnimationClip DashAnimation { get; set; }
    
    [OdinSerialize, Required] TriggerZone ActivationZone { get; set; } = new BoxTriggerZone();

    WalkingBehaviour WalkingBehaviour = new WalkingBehaviour {
        RunSpeed = 1.8f,
        AirRunSpeed = 1.8f,
        JumpSpeed = 1.8f,
        IdleMotion = new BasicMotion(0.5f),
        RunMotion = new BasicMotion(1f),
        InAirMotion = new BasicMotion(0.1f)
    };

    WallJumpBehaviour WallJumpBehaviour = new WallJumpBehaviour {
        JumpTime = 0.2f,
        DelayPerSide = 0.6f,
        JumpVelocity = new Vector2(1f, 1.8f),
        OnWallMotion = new BasicMotion(0.2f),
        WallJumpMotion = new BasicMotion(0.1f)
    };

    DashingBehaviour DashingBehaviour = new DashingBehaviour {
        DashSpeed = 6f,
        DashTime = 0.15f,
        Motion = new BasicMotion(0.1f),
        VerticalSpeedMultiplier = 0f
    };

    [OdinSerialize, Required] AdvancedBehaviour ShootingBehaviour = new AdvancedBehaviour {
        Motion = new AdvancedMotion(2f)
    };

    protected override void OnUpdate() {
        var input = Game.Instance.InputManager;

        var triggers = this.ActivationZone.DetectTriggers(this.Puppet.Body.Position, this.Puppet.Facing);

        if (this.DashingBehaviour.IsDashing) {
            if (this.Puppet.FrontIsTouchingWall) {
                this.DashingBehaviour.StopDashing();
            } else {
                return;
            }
        } 
        if (this.WallJumpBehaviour.IsWallJumping) {
            if (this.FrontIsOnWall) {
                if (input.ButtonDown("A")) {
                    this.WallJumpBehaviour.WallJump();
                    return;
                }
            }
            return;
        }
        
        var free = this.Puppet.CurrentMotion != this.ShootingBehaviour.Motion || this.Puppet.CurrentMotionHasFinished;
        
        if (free) {

            {
                this.UseAimRotation = true;
                var rb = input.ButtonHeld("Right Bumper");
                var lb = input.ButtonHeld("Left Bumper");
                if (rb && lb) {
                    this.AimDirection = Vector2.up;
                } else if (rb) {
                    this.AimDirection = (Vector2.down + Vector2.right).normalized;
                } else if (lb) {
                    this.AimDirection = (Vector2.up + Vector2.right).normalized;
                } else {
                    this.AimDirection = Vector2.right;
                    this.UseAimRotation = false;
                }
            }

            if (this.IsGrounded || this.IsOnWall) {
                this.DashingBehaviour.EnableDashing();
            }
            
            if (this.DashingBehaviour.CanDash) {
                if (input.HasAnalogLeftX && input.ButtonDown("B")) {
                    this.DashingBehaviour.Dash(input.AnalogLeft, Mathf.Abs(input.AnalogLeft.x));
                    return;
                }
            }
            
            if (input.ButtonDown("Y")) {
                this.ShootingBehaviour.Do(input.AnalogLeft);
                return;
            }
                
            if (this.IsGrounded) {
                if (input.ButtonDown("A")) {
                    this.WalkingBehaviour.Jump(Vector2.up);
                    return;
                }
            }

            if (this.FrontIsOnWall) {
                if (input.ButtonDown("A")) {
                    this.WallJumpBehaviour.WallJump();
                    return;
                }
            }
        }
        
        if (this.IsGrounded) {
            if (input.HasAnalogLeftX) {
                this.WalkingBehaviour.Run(input.AnalogLeftDirection.ToHorizontalDirection(), Math.Abs(input.AnalogLeft.x), free);
            } else {
                this.WalkingBehaviour.Stop(free);
            }
        } else if (this.IsInAir) {
            if (input.HasAnalogLeftX) {
                this.WalkingBehaviour.AirRun(input.AnalogLeftDirection.ToHorizontalDirection(), Mathf.Abs(input.AnalogLeft.x), free);
            } else {
                this.WalkingBehaviour.AirStop(free);
            }
            if (this.Puppet.Body.Velocity.y > 0f) {
                if (input.ButtonHeld("A")) {
                    this.Puppet.Body.Velocity += Vector2.up * JumpAddedAcceleration * Timing.Delta;
                }
            }
        }
    }

    protected override void OnAnimate(Motion motion) {
        var input = Game.Instance.InputManager;
        this.Puppet.Link(this.WalkingBehaviour.IdleMotion, this.IdleAnimation);
        this.Puppet.Link(this.WalkingBehaviour.RunMotion, this.RunAnimation, Mathf.Abs(input.AnalogLeft.x));
        this.Puppet.Link(this.WalkingBehaviour.InAirMotion, this.InAirAnimation);
        this.Puppet.Link(this.WalkingBehaviour.AirRunMotion, this.InAirAnimation);
        this.Puppet.Link(this.ShootingBehaviour.Motion, this.ShootingAnimation);
        this.Puppet.Link(this.DashingBehaviour.Motion, this.DashAnimation);
        this.Puppet.Link(this.WallJumpBehaviour.OnWallMotion, this.OnWallAnimation);
        this.Puppet.Link(this.WallJumpBehaviour.WallJumpMotion, this.InAirAnimation);
    }
}
