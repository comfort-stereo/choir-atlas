﻿using UnityEngine;
using Utilities;

public class Useless : Entity {
    
    public HitBehaviour HitBehaviour { get; } = new HitBehaviour {
        HeavyHitRatio = 0.5f,
        Poise = 0f,
        LightHitMotion = new BasicMotion(1f)
    };
    
    public WalkingBehaviour Walking { get; } = new WalkingBehaviour {
        RunSpeed = 1f,
        AirRunSpeed = 1f,
        JumpSpeed = 1f,
        IdleMotion = new BasicMotion(1f),
        RunMotion = new BasicMotion(1f),
        InAirMotion = new BasicMotion(1f)
    };

    HorizontalDirection MovementDirection { get; set; } = HorizontalDirection.Right;

    protected override void OnUpdate() {
        var current = this.Puppet.CurrentMotion;
        if (current != null && (current == this.HitBehaviour.LightHitMotion || current == this.HitBehaviour.HeavyHitMotion)) {
            ClearSustainedAction();
        }
    }
    
    protected override void OnAI() {
        var current = this.Puppet.CurrentMotion;
        
        if (current != null && (current == this.HitBehaviour.LightHitMotion || current == this.HitBehaviour.HeavyHitMotion)) {
            return;
        }
            
        if (this.IsGrounded) {
            if (ChancePerSecond(0.5f)) {
                this.MovementDirection = this.MovementDirection.Opposite();
            }
            if (ChancePerSecond(0.5f)) {
                this.Walking.Jump(Vector2.up);
            } else {
                SustainAction(entity => entity.ApplyAs<Entity, Useless>(self => {
                    self.Walking.Run(this.MovementDirection);
                }));
            }
        } else {
            SustainAction(entity => entity.ApplyAs<Entity, Useless>(self => {
                    
            }));
        }
    }

    protected override void OnAnimate(Motion motion) {
        this.Puppet.Animate(this.IdleAnimation, this.Walking.IdleMotion.Duration);
    }
}
